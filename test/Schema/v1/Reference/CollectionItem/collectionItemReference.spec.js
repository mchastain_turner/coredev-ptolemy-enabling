import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/CollectionItem/CollectionItemReference';

import article from '../article.fixture.json';
import collection from '../collection.fixture.json';
import coverageContainer from '../coverageContainer.fixture.json';
import gallery from '../gallery.fixture.json';
import image from '../image.fixture.json';
import profile from '../profile.fixture.json';
import section from '../section.fixture.json';
import video from '../video.fixture.json';
import videoCollection from '../videoCollection.fixture.json';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

let resolved = {
    id: 'video_test',
    type: 'reference',
    referenceType: 'video',
    reference: {
        id: 'video_test',
        type: 'video'
    }
};

function getResolved() {
    return resolved;
}

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('article_test')
    .returns(article[0]);
loadStub.withArgs('collection_test')
    .returns(collection[0]);
loadStub.withArgs('coverageContainer_test')
    .returns(coverageContainer[0]);
loadStub.withArgs('gallery_test')
    .returns(gallery[0]);
loadStub.withArgs('image_test')
    .returns(image[0]);
loadStub.withArgs('profile_test')
    .returns(profile[0]);
loadStub.withArgs('section_test')
    .returns(section[0]);
loadStub.withArgs('video_test')
    .returns(video[0]);
loadStub.withArgs('videoCollection_test')
    .returns(videoCollection[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }
    },
    log: logStub
};

const query = `
    query CollectionItemReference {
        collectionReference {
            id
            type
            referenceType
            reference {
                ...on Article {
                    id
                    type
                }
                ...on Collection {
                    id
                    type
                }
                ...on CoverageContainer {
                    id
                    type
                }
                ...on Gallery {
                    id
                    type
                }
                ...on Image {
                    id
                    type
                }
                ...on Profile {
                    id
                    type
                }
                ...on Section {
                    id
                    type
                }
                ...on Video {
                    id
                    type
                }
                ...on VideoCollection {
                    id
                    type
                }
            }
        }
    }
`;

describe('RelatedMediaItem VideoReference Schema Tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            collectionReference: {
                type: TypeSchema,
                resolve: getResolved
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    describe('Hydrating reference', function() {
        it('should return a article as the hydrated reference', async function() {
            const type = 'article';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a collection as the hydrated reference', async function() {
            const type = 'collection';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a coverageContainer as the hydrated reference', async function() {
            const type = 'coverageContainer';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a gallery as the hydrated reference', async function() {
            const type = 'gallery';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a image as the hydrated reference', async function() {
            const type = 'image';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a profile as the hydrated reference', async function() {
            const type = 'profile';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a section as the hydrated reference', async function() {
            const type = 'section';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a video as the hydrated reference', async function() {
            const type = 'video';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return a videoCollection as the hydrated reference', async function() {
            const type = 'videoCollection';
            resolved = {
                id: `${type}_test`,
                type: 'reference',
                referenceType: type,
                reference: {
                    id: `${type}_test`,
                    type
                }
            };

            const expected = {
                data: {
                    collectionReference: resolved
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });
    });
});
