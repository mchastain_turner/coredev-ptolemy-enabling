import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './gallery.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Gallery';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub
    }
};

describe('Didgeridoo Gallery Schema Tests', function() {
    describe('Basic Gallery schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                gallery: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an gallery', async function() {
            const query = `
                query Gallery {
                    gallery {
                        id
                        type
                    }
                }
            `;

            const expected = {
                gallery: {
                    id: 'gallery_test',
                    type: 'gallery'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an gallery', async function() {
            const query = `
                query Gallery {
                    gallery {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        title
                        description {
                            id
                            plaintext
                            richtext
                            elements {
                                ...on Handle {
                                    type
                                    attributes
                                    target {
                                        type
                                        subtype
                                        referenceUrl
                                        referenceUri
                                    }
                                }
                                ...on Embed {
                                    type
                                    attributes
                                }
                            }
                        }
                        attributes
                        source
                        branding
                        section
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        slides {
                            type
                            caption {
                                id
                                plaintext
                                richtext
                                elements {
                                    ...on Handle {
                                        type
                                        attributes
                                        target {
                                            type
                                            subtype
                                            referenceUrl
                                            referenceUri
                                        }
                                    }
                                    ...on Embed {
                                        type
                                        attributes
                                    }
                                }
                            }
                            headline
                            source
                            credit
                            thumbText
                            format
                            image {
                                url
                            }
                            cuts
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideoCollection
                            hasInteractive
                            media {
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    type
                                    headline
                                    description {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                }
                                ...on ImageValue {
                                    id
                                    imageId
                                    type
                                    referenceUrl
                                    referenceUri
                                    location
                                    slug
                                    dam_id
                                    photographer
                                    caption
                                    cuts
                                }
                            }
                            autoZone {
                                auto
                            }
                        }
                        parent { id }
                        language
                        rights {
                            policyid
                            policytype
                            permissions {
                                action
                                assignee
                                target
                            }
                            prohibitions {
                                action
                                assignee
                                target
                            }
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    gallery: fixture[0]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return specific cuts on a gallery slides', async function() {
            const query = `
                query Gallery {
                    gallery {
                        id
                        slides {
                            cuts(sizes:["medium4to3"])
                        }
                    }
                }
            `;

            const expectedCuts = fixture[0].slides
                .map((slide) => {
                    return {
                        cuts: {
                            medium4to3: slide.cuts.medium4to3
                        }
                    };
                });

            const expected = {
                data: {
                    gallery: {
                        id: fixture[0].id,
                        slides: expectedCuts
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });


    });

});
