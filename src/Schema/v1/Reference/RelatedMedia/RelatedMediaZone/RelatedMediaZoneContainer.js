/* eslint max-len:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLBoolean,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import RelatedMediaItem from '../RelatedMediaItem';
import Reference from '../../../Reference';


const RelatedMediaZoneContainer = new GraphQLObjectType({
    name: 'RelatedMediaZoneContainer',
    fields: () => ({
        branding: {
            description: 'The branding to use when this container is rendered',
            type: GraphQLString
        },
        brandingOn: {
            description: 'Wheter or not to use branding',
            type: GraphQLBoolean
        },
        dateCreated: {
            description: 'Date this container was added',
            type: GraphQLString
        },
        id: {
            description: 'Unique identifier for this container instance',
            type: GraphQLString
        },
        label: {
            description: 'The label to display',
            type: GraphQLString
        },
        lastModifiedDate: {
            description: 'Last time this container was modified',
            type: GraphQLString
        },
        lastModifiedUser: {
            description: 'Last user to modify this section',
            type: GraphQLString
        },
        layout: {
            description: 'Expected layout for rendering',
            type: GraphQLString
        },
        parent: {
            description: 'The zone which contains this container',
            type: GraphQLString
        },
        type: {
            description: 'The type of content this container references',
            type: GraphQLString
        },
        weight: {
            description: 'The index of this container in its parent',
            type: GraphQLInt
        },
        zoneType: {
            description: 'What type of zone this is: container or card',
            type: GraphQLString
        },
        intlAutoplay: {
            description: 'If this is playlist, enable auto play for international users',
            type: GraphQLBoolean
        },
        autoplay: {
            description: 'If this is playlist, enable auto play for users',
            type: GraphQLBoolean
        },
        newWindow: {
            description: 'If this contains a hyperlink target, controls if this link opens a window',
            type: GraphQLBoolean
        },
        children: {
            description: 'The content cards that this zone contains',
            type: new GraphQLList(RelatedMediaItem)
        },
        target: {
            type: Reference,
            description: 'The target attached to the container, resolved to the type',
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'target');
            }
        }
    })
});

export default RelatedMediaZoneContainer;
