import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import Parent from './Subs/Parent';
import Topic from './Subs/Topic';
import { ObjectType } from '../ObjectType';
import RelatedMedia from './RelatedMedia';
import CollectionSlide from './CollectionItem/CollectionSlide';
import Rights from './Rights';

const Gallery = new GraphQLObjectType({
    name: 'Gallery',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },

        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        title: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        source: { type: GraphQLString },
        branding: { type: GraphQLString },
        section: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        slides: {
            type: new GraphQLList(CollectionSlide)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        parent: {
            type: Parent
        },
        language: { type: GraphQLString },
        rights: {
            type: new GraphQLList(Rights)
        }
    })
});

export default Gallery;
