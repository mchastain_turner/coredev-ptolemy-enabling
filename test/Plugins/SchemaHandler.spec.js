import chai from 'chai';
import {
    stub,
    match
} from 'sinon';
import SchemaHandler from '../../src/Plugins/SchemaHandler';

chai.should();

describe('SchemaHandler', function() {
    describe('graphql plugin registration', () => {
        it('should register the plugin properly', function(done) {
            const registerStub = stub();
            const authStartegyStub = stub();

            const stubServer = {
                register: registerStub,
                auth: {strategy: authStartegyStub},
                log: stub()
            };

            SchemaHandler.register(stubServer, {
                    bearerToken: '12345',
                    db: { foo: 'bar' }
                },
                () => {

                    authStartegyStub.should.have.been.calledTwice
                        .and.calledWith('bearer', 'bearer-access-token', {
                            allowQueryToken: false,
                            validateFunc: match.func
                        })
                        .and.calledWith('simple', 'basic', {
                            validateFunc: match.func
                        });

                    registerStub.secondCall.args[0].should.have.property('options')
                        .and.have.property('path', '/svc/ptolemy/v1/query');

                    registerStub.getCall(3).args[0].should.have.property('options')
                        .and.have.property('path', '/svc/ptolemy/v1');

                    done();
                }
            );
        });

    });

    describe('auth strategy', () => {
        const validateFunc = {};
        const stubServer = {
            register: stub(),
            auth: {
                strategy: (name, plugin, opts) => {
                    validateFunc[name] = opts.validateFunc;
                }},
            route: stub(),
            log: stub()
        };

        it('should validate bearer tokens', function(done) {
            SchemaHandler.register(stubServer, {
                    bearerToken: '12345'
                },
                () => {
                    validateFunc.bearer('12345', (_, isValid, info) => {
                        try {
                            isValid.should.be.true;
                            info.should.have.property('token', '12345');
                            done();
                        } catch(e) {
                            done(e);
                        }
                    });
                }
            );
        });

        it('should validate simple auth requests with a token', function(done) {
            SchemaHandler.register(stubServer, {
                    bearerToken: '12345'
                },
                () => {
                    validateFunc.simple(null, '12345', '', (_, isValid, info) => {
                        try {
                            isValid.should.be.true;
                            info.should.have.property('token', '12345');
                            done();
                        } catch(e) {
                            done(e);
                        }
                    });
                }
            );
        });

        it('should fail validation if simple auth requests with the wrong token', function(done) {
            SchemaHandler.register(stubServer, {
                    bearerToken: '12345'
                },
                () => {
                    validateFunc.simple(null, '6789', '', (_, isValid, info) => {
                        try {
                            isValid.should.be.false;
                            (!info).should.be.true;
                            done();
                        } catch(e) {
                            done(e);
                        }
                    });
                }
            );
        });

    });

    describe('injectToken', function() {

        const reply = stub();
        reply.prototype.continue = stub();

        it('should inject the token into the header if auth exists', function(done) {
            const expected =
                '<head><script>window.__TOKEN = "12345";</script></head>';

            SchemaHandler.injectToken(
                {
                    auth: {
                        credentials: {
                            token: '12345'
                        }
                    },
                    response: {
                        source: '<head></head>'
                    }
                }, (body) => {
                body.should.equal(expected);
                done();
            });
        });

        it('should just continue if there is an error', function() {

            const reply = {
                continue: stub()
            };

            SchemaHandler.injectToken(
                {
                    auth: {
                        credentials: {
                            token: '12345'
                        }
                    },
                    response: {
                        isBoom: true
                    }
                },
                reply
            );

            reply.continue.should.have.been.calledOnce;

        });

        it('should continue if there is no token', function() {

            const reply = {
                continue: stub()
            };

            SchemaHandler.injectToken(
                {
                    auth: {
                        credentials: null
                    },
                    response: {
                        source: '<head></head>'
                    }
                },
                reply
            );

            reply.continue.should.have.been.calledOnce;

        });

    });

});
