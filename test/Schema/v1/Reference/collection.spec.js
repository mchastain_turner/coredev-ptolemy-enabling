import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import collectionFixture from './collection.fixture.json';
import collectionExpected from './collection.expected.json';
import video from './video.fixture.json';
import gallery from './gallery.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Collection';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

let fixture;

const logStub = stub();
const loadStub = stub();

loadStub.withArgs('gallery_test')
    .returns(gallery[0]);

loadStub.withArgs('video_test')
    .returns(video[0]);

const qopts = {
    db: {
        docLoader: {
            load: loadStub
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }
    },
    log: logStub
};

describe('Didgeridoo Collection Schema Tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            collection: {
                type: TypeSchema,
                resolve() {
                    return fixture;
                }
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    describe('Basic Collection schema test', function() {
        it('should return a field on an collection', async function() {
            fixture = collectionFixture[0];
            const query = `
                query Collection {
                    collection {
                        id
                        type
                    }
                }
            `;

            const expected = {
                collection: {
                    id: 'collection_test',
                    type: 'collection'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an collection', async function() {
            fixture = collectionFixture[0];
            const query = `
                query Collection {
                    collection {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        title
                        description {
                            ...ParagraphFragment
                        }
                        attributes
                        section
                        sponsorship
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        content {
                            id
                            type
                            location
                            url
                            headline
                            shortheadline
                            description {
                                ...ParagraphFragment
                            }
                            referenceType
                            referenceUrl
                            referenceUri
                            reference {
                                ...on Video{
                                    id
                                    type
                                }
                                ...on Gallery{
                                    id
                                    type
                                }
                                ...on EmbedValue{
                                    type
                                    slug
                                }
                            }
                            sendToApps
                            cuts
                        }
                        autoZone { auto }
                        collectionFormat
                        language
                    }
                }
                fragment ParagraphFragment on Paragraph {
                    id
                    plaintext
                    richtext
                    elements {
                        ...on Handle {
                            type
                            attributes
                            target {
                                type
                                subtype
                                referenceUrl
                                referenceUri
                            }
                        }
                        ...on Embed {
                            type
                            attributes
                        }
                    }
                    format
                }
            `;
            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(collectionExpected);
        });
    });

    describe('Collection format schema test', function() {

        it('should return null for format on a collection with no format field', async function() {
            fixture = {
                description: [
                    {
                        id: 'foo',
                        plaintext: 'test plain text',
                        richtext: null,
                        elements: []
                    }
                ]
            };

            const query = `
                query Collection {
                    collection {
                        description {
                            id
                            format
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    collection: {
                        description: [
                            {
                                id: 'foo',
                                format: null
                            }
                        ]
                    }
                }
            };


            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return format on a collection with a format field', async function() {
            fixture = {
                description: [
                    {
                        id: 'foo',
                        plaintext: 'test plain text',
                        richtext: null,
                        elements: [],
                        format: []
                    }
                ]
            };

            const query = `
                query Collection {
                    collection {
                        description {
                            id
                            format
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    collection: {
                        description: [
                            {
                                id: 'foo',
                                format: []
                            }
                        ]
                    }
                }
            };


            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });
    });

});
