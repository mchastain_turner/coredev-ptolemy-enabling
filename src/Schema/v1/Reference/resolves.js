
export function resolveCuts(parent, {sizes}) {
    if (parent.cuts && sizes) {
        const cuts = {};
        Object.keys(parent.cuts).filter(function(el) {
            if (sizes.indexOf(el) >= 0) {
                cuts[el] = parent.cuts[el];
            }
        });
        return cuts;
    } else {
        return parent.cuts;
    }
}

/*
 * This will retrieve the subproperties of the given Object.
 *
 * If the subObjectPath is given it will retrieve properties of the sub-object
 * of the parent.
 *
 * If subObjectPath is set to "foo.bar" then the returned closure will return
 * the properties of the subObject "parent.foo.bar"
 *
 * If subObjectPath is set to falsy it will get the properties of the parent
 * object
 *
 * subObjectPath must be a string or falsy
 *
 */
export function resolvePropertiesFactory(subObjectPath) {
    if (subObjectPath && typeof (subObjectPath) !== 'string') {
        throw new Error('subObject must be a string or falsy');
    }
    const subObjects = subObjectPath.split('.');
    return (parent, {properties}) => {
        let object = parent;
        if (parent && subObjects) {
            for (let i = 0, l = subObjects.length; i < l; i++) {
                object = object[subObjects[i]];
                // Further subproperty resolution will fail on a falsy value.
                // Return the falsy statement and stop trying to resolve
                // the path
                if (!object) {
                    return object;
                }
            }
        }

        if (properties) {

            const subProperties = {};
            Object.keys(object).filter(function(el) {
                if (properties.indexOf(el) >= 0) {
                    subProperties[el] = object[el];
                }
            });
            return subProperties;
        } else {
            return object;
        }
    };
}
