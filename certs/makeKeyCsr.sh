#!/usr/bin/env bash

FQDN=$1

openssl req -newkey rsa:2048 -nodes \
    -keyout ${FQDN}.key \
    -out ${FQDN}.csr \
    -subj "/C=US/ST=GA/L=Atlanta/O=Turner Broadcasting System, Inc/OU=MSS/CN=${FQDN}"
