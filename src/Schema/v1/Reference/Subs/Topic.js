import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean,
    GraphQLFloat
} from 'graphql';

const Topic = new GraphQLObjectType({
    name: 'Topic',
    fields: () => ({
        label: { type: GraphQLString },
        class: { type: GraphQLString },
        id: { type: GraphQLString },
        topicID: { type: GraphQLString },
        confidenceScore: { type: GraphQLFloat },
        leafNode: { type: GraphQLBoolean },
        parentPathIds: {
            type: new GraphQLList(
                new GraphQLList(
                    GraphQLString
                )
            )
        }
    })
});

export default Topic;
