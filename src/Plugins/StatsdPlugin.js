import {StatsD} from 'node-dogstatsd';


function register(server, options, next) {
    const client = new StatsD(options.hostname, options.port);
    const defaultTags = options.tags;
    const prefix = options.prefix;

    function getAllTags(tags) {
        return (tags && tags.length ? defaultTags.concat(tags) : defaultTags);
    }

    server.plugins.statsdClient = {
        increment(key, tags) {
            client.increment(
                `${prefix}.${key}`,
                getAllTags(tags)
            );
        },
        histogram(key, value, tags) {
            client.historgram(
                `${prefix}.${key}`,
                value,
                getAllTags(tags)
            );
        },
        timing(key, value, tags) {
            client.timing(
                `${prefix}.${key}`,
                value,
                getAllTags(tags)
            );
        },
        gauge(key, value, tags) {
            client.gauge(
                `${prefix}.${key}`,
                value,
                getAllTags(tags)
            );
        }
    };

    server.on('response', function(request) {
        const response = request.response;
        const tags = defaultTags.concat([`path:${request.path}`]);
        if ( response.isBoom ) {
            client.increment(`${prefix}.route.failed`,
                tags
            );
        } else {
            client.increment(`${prefix}.route.success`,
                tags
            );
        }

        client.histogram(`${prefix}.response.time`,
            request.info.responded - request.info.received,
            tags
        );
    });

    return next();
}

register.attributes = {
    name: 'StasdPlugin'
};

const StatsdPlugin = {register};
export default StatsdPlugin;
