import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} from 'graphql';

const Cut = new GraphQLObjectType({
    name: 'Cut',
    fields: () => ({
        height: { type: GraphQLInt },
        width: { type: GraphQLInt },
        url: { type: GraphQLString }
    })
});

export default Cut;
