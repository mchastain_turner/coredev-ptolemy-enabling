import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLBoolean
} from 'graphql';

import Paragraph from './Paragraph';
import { ObjectType } from './ObjectType';
import Reference from './Reference';
import { resolveCuts } from './Reference/resolves';
import ExtensionAttributes from
'./Reference/RelatedMedia/RelatedMediaItem/Types/ExtensionAttributes';

const Card = new GraphQLObjectType({
    name: 'Card',
    fields: () => ({
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        description: {
            type: new GraphQLList(Paragraph)
        },
        headline: { type: GraphQLString },
        location: { type: GraphQLString },
        referenceType: {
            type: GraphQLString,
            deprecationReason:
                'Moving reference to target and removing this field'
        },
        type: { type: GraphQLString },
        url: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        sendToApps: { type: GraphQLBoolean },
        banner: {
            type: new GraphQLList(Paragraph)
        },
        wordCount: { type: GraphQLInt },
        storyType: { type: GraphQLString },
        extensionAttributes: { type: ExtensionAttributes },
        referenceUrl: {
            type: GraphQLString,
            deprecationReason:
                'Moving reference to target and removing this field'
        },
        referenceId: {
            type: GraphQLID,
            deprecationReason:
                'Moving reference to target and removing this field'
        },
        target: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'target', parent.referenceId);
            }
        },
        reference: {
            type: Reference,
            deprecationReason:
                'Moving reference to target in order to support value types',
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'target', parent.referenceId);
            }
        },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default Card;
