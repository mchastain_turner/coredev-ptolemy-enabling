import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import {
    cleanTarget
} from '../../../helper';

import fixture from './section.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Section';

const expectedFixture = JSON.parse(JSON.stringify(fixture));
expectedFixture[0].relatedMedia.media[52].target = expectedFixture[0].relatedMedia.media[52].target.handle;
cleanTarget(expectedFixture[0].relatedMedia.media[52].target);

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDocs: getStub
    }
};

describe('Didgeridoo Section Schema Tests', function() {
    describe('Basic Section schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                section: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an section', async function() {
            const query = `
                query Section {
                    section {
                        id
                        type
                    }
                }
            `;

            const expected = {
                section: {
                    id: 'section_test',
                    type: 'section'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an section', async function() {
            const query = `
                query Section {
                    section {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        url
                        attributes
                        branding
                        section
                        sectionDisplayName
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideoCollection
                            hasInteractive
                            zone {
                                limit
                                zoneType
                                displayLabel
                                label
                                showAds
                                id
                                priority
                                branding
                                options
                            }
                            media {
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    type
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                }
                                ...on ImageValue {
                                    id
                                    imageId
                                    type
                                    referenceUrl
                                    referenceUri
                                    location
                                    slug
                                    dam_id
                                    photographer
                                    caption
                                    cuts
                                }
                                ...on CoverageContainerReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on HyperlinkReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on InteractiveReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on GalleryReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                                ...on ArticleReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                                ...on ValueCard {
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    banner {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                        format
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                    target{
                                        ...on AnimationValue{
                                            caption
                                            type
                                            lastModifiedUser
                                            mediaType
                                            lastModifiedDate
                                            filename
                                            dateCreated
                                            slug
                                            altTag
                                            url
                                        }
                                  }
                                  extensionAttributes{
                                        branding
                                        captions
                                        dateCreated
                                        flag
                                        flagColor
                                        kicker{
                                            id
                                            plaintext
                                            richtext
                                            elements {
                                                ...on Handle {
                                                    type
                                                    attributes
                                                    target {
                                                        type
                                                        subtype
                                                        referenceUrl
                                                        referenceUri
                                                    }
                                                }
                                                ...on Embed {
                                                    type
                                                    attributes
                                                }
                                            }
                                            format
                                        }
                                    }
                                 valueType
                                }
                            }
                            autoZone {
                                auto
                            }
                        }
                        language
                    }
                }
                fragment ParagraphFragment on Paragraph {
                    id
                    plaintext
                    richtext
                    elements {
                        ...on Handle {
                            type
                            attributes
                            target {
                                type
                                subtype
                                referenceUrl
                                referenceUri
                            }
                        }
                        ...on Embed {
                            type
                            attributes
                        }
                    }
                }
                fragment ParagraphFormatFragment on Paragraph {
                    ...ParagraphFragment
                    format
                }
            `;
            const expectedFixtureCopy = JSON.parse(JSON.stringify(expectedFixture[0]));
            expectedFixtureCopy.relatedMedia.zone = Object.keys(expectedFixtureCopy.relatedMedia.zone)
                .map((key) => expectedFixtureCopy.relatedMedia.zone[key]);
            const expected = {
                data: {
                    section: expectedFixtureCopy
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });


    });

});
