import {
    GraphQLUnionType
} from 'graphql';

import Embed from './Embed';
import Handle from './Handle';
import UnknownType from '../Reference/UnknownType';

const Elements = new GraphQLUnionType({
    name: 'Elements',
    types: [
        Embed,
        Handle,
        UnknownType
    ],
    resolveType(obj, _, {rootValue: {log}}) {
        switch(obj.type) {
            case 'embed': return Embed;
            case 'handle': return Handle;
            default:
                log(['warn', 'uknownType'],
                    `Unknown or unsuported 'Elements' id: "${obj.id}", ` +
                    `type: "${obj.type}"`
                );
                return UnknownType;
        }
    }
});

export default Elements;
