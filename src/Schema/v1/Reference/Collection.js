import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import AutoZone from './Subs/AutoZone';
import Topic from './Subs/Topic';
import { ObjectType } from '../ObjectType';
import CollectionItemReference from './CollectionItem/CollectionItemReference';

const Collection = new GraphQLObjectType({
    name: 'Collection',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        title: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        section: { type: GraphQLString },
        sponsorship: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        content: {
            type: new GraphQLList(CollectionItemReference)
        },
        autoZone: {
            type: AutoZone
        },
        collectionFormat: { type: GraphQLString },
        language: { type: GraphQLString }
    })
});

export default Collection;
