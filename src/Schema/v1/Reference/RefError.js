import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const RefError = new GraphQLObjectType({
    name: 'RefError',
    fields: () => ({
        id: { type: GraphQLString },
        type: { type: GraphQLString },
        error: { type: GraphQLString }
    })
});

export default RefError;
