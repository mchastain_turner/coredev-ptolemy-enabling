import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';
import {
    ObjectType
} from '../../../../../../ObjectType';
import {
    resolvePropertiesFactory
} from '../../../../../resolves';
const resolveOembed = resolvePropertiesFactory('oembed');
const EmbedValue = new GraphQLObjectType({
    name: 'EmbedValue',
    fields: () => ({
        tags: {
            type: new GraphQLList(GraphQLString)
        },
        oembed: {
            type: ObjectType,
            args: {
                properties: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveOembed
        },
        type: {
            type: GraphQLString
        },
        mediaType: {
            type: GraphQLString
        },
        url: {
            type: GraphQLString
        },
        lastModifiedUser: {
            type: GraphQLString
        },
        lastModifiedDate: {
            type: GraphQLString
        },
        dateCreated: {
            type: GraphQLString
        },
        slug: {
            type: GraphQLString
        },
        attributes: {
            type: ObjectType,
            resolve(resource) {
                return resource.attributes;
            }
        }
    })
});
export default EmbedValue;
