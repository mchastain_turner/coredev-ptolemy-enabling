import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/CoverageContainerReference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import coverageContainer from '../coverageContainer.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('coverageContainer_test')
    .returns(coverageContainer[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent) {
            return loadStub(parent.resource.handle.referenceId);
        }
    },
    log: logStub
};

describe('RelatedMediaItem CoverageContainerReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                coverageContainerReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'coverageContainer_test',
                            referenceId: 'coverageContainer_test',
                            type: 'reference',
                            referenceType: 'coverageContainer',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the coverageContainer as a reference', async function() {
            const query = `
                query CoverageContainerReference {
                    coverageContainerReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    coverageContainerReference: {
                        referenceId: 'coverageContainer_test',
                        type: 'reference',
                        referenceType: 'coverageContainer',
                        reference: {
                            id: 'coverageContainer_test',
                            type: 'coverageContainer'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
