import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/Article';
import IndexSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import relatedMedia from './relatedMedia.fixture.json';
import relatedMediaZone from './relatedMediaZone.fixture.json';
import expectedRelatedMediaZone from './relatedMediaZone.expected.json';
import video from '../video.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();
let loadStub;

beforeEach(function() {
    loadStub = stub();
    loadStub.withArgs('video_test')
        .returns(video[0]);

    loadStub.withArgs('image_test')
        .returns(image[0]);
});

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }
    },
    log: logStub
};

describe('RelatedMedia resolve tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            relatedMedia: {
                type: IndexSchema,
                resolve() {
                    return relatedMedia;
                }
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    it('should resolve the media items', function(done) {
        const query = `
            query RelatedMedia {
                relatedMedia {
                    media {
                        ...on VideoReference {
                            id
                            type
                            reference {
                                ...on Video {
                                    id
                                    type
                                }
                            }
                            resource {
                                ...on Image {
                                    id
                                    type
                                }
                            }
                        }
                        ...on ImageValue {
                            id
                            imageId
                            type
                        }
                        ...on InteractiveValue {
                            type
                            subtype
                        }
                        ...on ValueCard {
                            type
                            valueType
                            target {
                                ...on EmbedValue {
                                    type
                                    attributes
                                }
                            }
                        }
                        ...on ValueElement {
                            type
                            elementType
                            target {
                                ...EmbedFragment
                            }
                        }
                    }
                }
            }
            fragment EmbedFragment on EmbedValue {
                type
                attributes
            }
        `;

        const expected = {
            relatedMedia: {
                media: [
                    {
                        id: 'video_test',
                        type: 'reference',
                        reference: {
                            id: 'video_test',
                            type: 'video'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    },
                    {
                        id: 'image_test',
                        imageId: 'image_test',
                        type: 'image'
                    },
                    {
                        subtype: 'webtag',
                        type: 'interactive'
                    },
                    {
                        type: 'value',
                        valueType: 'embed',
                        target: {
                            type: 'embed',
                            attributes: null
                        }
                    },
                    {
                        elementType: 'animation',
                        type: 'element',
                        target: {}
                    },
                    {
                        type: 'element',
                        elementType: 'legacyEmbed',
                        target: {
                            attributes: {
                                description: 'mcgraw',
                                type: 'youtube',
                                url: '//www.youtube.com/embed/MpbIQGmvS2Y'
                            },
                            type: 'embed'
                        }
                    }
                ]
            }
        };

        graphql(Schema, query, qopts)
            .then(function(result) {
                result.should.to.deep.equal( { data: expected });
                done();
            })
            .catch(done);
    });

    it('should only call for items whose types were requested', function(done) {
        const fixture = {
            media: [
                {
                    id: 'video_test',
                    referenceId: 'video_test',
                    type: 'reference',
                    referenceType: 'video'
                },
                {
                    id: 'article_test',
                    referenceId: 'article_test',
                    type: 'reference',
                    referenceType: 'article'
                },
                {
                    id: 'image_test',
                    type: 'image'
                }
            ]
        };

        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                relatedMedia: {
                    type: IndexSchema,
                    resolve() {
                        return fixture;
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });


        const query = `
            query RelatedMedia {
                relatedMedia {
                    media {
                        ...on VideoReference {
                            id
                            type
                            reference {
                                ...on Video {
                                    id
                                    type
                                }
                            }
                        }
                    }
                }
            }
        `;

        const expected = {
            relatedMedia: {
                media: [
                    {
                        id: 'video_test',
                        type: 'reference',
                        reference: {
                            id: 'video_test',
                            type: 'video'
                        }
                    },
                    {},
                    {}
                ]
            }
        };

        graphql(Schema, query, qopts)
            .then(function(result) {
                loadStub.should.have.been.calledOnce
                    .and.calledWith('video_test');
                result.should.to.deep.equal( { data: expected });
                done();
            })
            .catch(done);
    });
});

describe('RelatedMediaZone resolve tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            relatedMedia: {
                type: IndexSchema,
                resolve() {
                    return relatedMediaZone;
                }
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    it('should resolve the media, zone, and container items', function(done) {
        const query = `
            query RelatedMedia {
                relatedMedia {
                    media {
                        ...ReferenceType
                    }
                    zone {
                        id
                        label
                        zoneType
                        children {
                            ...ZoneChildren
                        }
                        target {
                            ...on Video {
                                id
                                type
                            }
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            }
            fragment ReferenceType on RelatedMediaItem {
                ...on VideoReference {
                    id
                    type
                    referenceType
                }
                ...on HyperlinkReference {
                    id
                    headline
                    referenceType
                }
                ...on ArticleReference {
                    id
                    type
                    referenceType
                }
                ...on CollectionReference {
                    id
                    type
                    referenceType
                }
                ...on GalleryReference {
                    id
                    type
                    referenceType
                }
                ...on ValueCard {
                    id
                    type
                    referenceType
                }
            }
            fragment ZoneChildren on RelatedMediaItem{
                ...on RelatedMediaZoneContainer {
                    label
                    target {
                        ...on LiveVideoValue {
                            liveStream
                            type
                        }
                        ...on HyperlinkValue {
                            type
                            url
                            text
                            newWindow
                        }
                        ...on Video {
                            id
                            type
                        }
                    }
                    children {
                        ...ReferenceType
                    }
                }
                ...on VideoReference {
                    id
                    type
                    referenceType
                }
                ...on HyperlinkReference {
                    id
                    headline
                    referenceType
                }
                ...on ArticleReference {
                    id
                    type
                    referenceType
                }
                ...on CollectionReference {
                    id
                    type
                    referenceType
                }
                ...on GalleryReference {
                    id
                    type
                    referenceType
                }
                ...on ValueCard {
                    id
                    type
                    referenceType
                }
            }

        `;

        const expected = expectedRelatedMediaZone;

        graphql(Schema, query, qopts)
            .then(function(result) {
                result.should.to.deep.equal( expected );
                done();
            })
            .catch(done);
    });
    it('should resolve the media, zone, and container items', function(done) {
        const query = `
            query RelatedMedia {
                relatedMedia {
                    zone(ids:"app-news-zone-1") {
                        id
                        label
                        zoneType
                    }
                }
            }`;
        const query2 = `
            query RelatedMedia {
                relatedMedia {
                    zone(ids:["app-news-zone-1","app-news-zone-2", "bad-id"]) {
                        id
                        label
                        zoneType
                    }
                }
            }`;

        const expected = {
            data: {
                relatedMedia: {
                    zone: [{
                        id: 'app-news-zone-1',
                        label: 'A1 and A2 stories or packages',
                        zoneType: 'card'
                    }]
                }
            }
        };
        const expected2 = {
            data: {
                relatedMedia: {
                    zone: [{
                        id: 'app-news-zone-1',
                        label: 'A1 and A2 stories or packages',
                        zoneType: 'card'
                    }, {
                        id: 'app-news-zone-2',
                        label: 'P1 through P10',
                        zoneType: 'card'
                    }]
                }
            }
        };
        graphql(Schema, query, qopts)
            .then(function(result) {
                result.should.to.deep.equal( expected );
            }).then(()=>{
                graphql(Schema, query2, qopts)
                    .then(function(result) {
                        result.should.to.deep.equal( expected2 );
                        done();
                    });
            })
            .catch(done);
    });

});

describe('RelatedMediaItem index Union tests', function() {
    describe('Reference type error conditions', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                article: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            relatedMedia: {
                                media: [
                                    {
                                        referenceId: 'bad_reference_type_test',
                                        type: 'reference',
                                        referenceType: 'foo'
                                    }
                                ]
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return an error on a bad reference type', async function() {
            const query = `
                query Article {
                    article {
                        relatedMedia {
                            media {
                                ...on ArticleReference {
                                    referenceId
                                    referenceType
                                    type
                                }
                                ...on UnknownType {
                                    referenceId
                                    referenceType
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    article: {
                        relatedMedia: {
                            media: [
                                {
                                    referenceId: 'bad_reference_type_test',
                                    referenceType: 'foo'
                                }
                            ]
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });
    });

    describe('Value type error conditions', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                article: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            relatedMedia: {
                                media: [
                                    {
                                        referenceId: 'bad_reference_type_test',
                                        type: 'value',
                                        valueType: 'foo'
                                    }
                                ]
                            }
                        };
                    }
                }
            })
        });


        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return an error on a bad value type', async function() {
            const query = `
                query Article {
                    article {
                        relatedMedia {
                            media {
                                ...on ValueCard {
                                    valueType
                                    type
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    article: {
                        relatedMedia: {
                            media: [{
                                valueType: null,
                                type: 'value'
                            }]
                        }
                    }
                },
                errors: [{
                    message: 'Expected a value of type "valueCardTypes" but received: foo',
                    locations: [{
                        line: 7,
                        column: 37
                    }],
                    path: ['article', 'relatedMedia', 'media', 0, 'valueType']
                }]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });
    });
});
