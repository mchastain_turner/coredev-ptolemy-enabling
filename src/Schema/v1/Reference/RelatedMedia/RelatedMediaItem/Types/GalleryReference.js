/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import {ObjectType} from '../../../../ObjectType';
import Paragraph from '../../../../Paragraph';
import Reference from '../../../../Reference';

import {
    resolveCuts
} from '../../../resolves';

import Gallery from '../../../Gallery';
import ExtensionAttributes from './ExtensionAttributes';


const GalleryReference = new GraphQLObjectType({
    name: 'GalleryReference',
    fields: () => ({
        id: { type: GraphQLString },
        referenceId: { type: GraphQLString },
        type: { type: GraphQLString },
        location: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        banner: { type: new GraphQLList(Paragraph) },
        url: { type: GraphQLString },
        referenceType: { type: GraphQLString },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        reference: {
            type: Gallery,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveRef(parent);
            }
        },
        sendToApps: { type: GraphQLBoolean },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        auxiliaryText: { type: GraphQLString },
        extensionAttributes: { type: ExtensionAttributes },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default GalleryReference;
