import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import Topic from './Subs/Topic';
import RelatedMedia from './RelatedMedia';
import { ObjectType } from '../ObjectType';

const Audio = new GraphQLObjectType({
    name: 'Audio',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        transcript: { type: new GraphQLList(Paragraph) },
        fileLocation: { type: GraphQLString },
        section: { type: GraphQLString },
        branding: { type: GraphQLString },
        show: { type: GraphQLString },
        podcastFeed: { type: GraphQLString },
        audioProfiles: {
            type: ObjectType
        },
        subsite: { type: GraphQLString },
        audioLength: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        language: { type: GraphQLString }
    })
});

export default Audio;
