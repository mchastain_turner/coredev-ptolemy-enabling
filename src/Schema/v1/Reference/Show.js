import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import Contributor from './Subs/Contributor';
import Topic from './Subs/Topic';
import RelatedMedia from './RelatedMedia';
import { ObjectType } from '../ObjectType';

const Show = new GraphQLObjectType({
    name: 'Show',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        showName: { type: GraphQLString },
        franchises: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        contributors: {
            type: new GraphQLList(Contributor)
        },
        branding: { type: GraphQLString },
        section: { type: GraphQLString },
        bannerTitles: {
            type: ObjectType
        },
        ads: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        language: { type: GraphQLString }
    })
});

export default Show;
