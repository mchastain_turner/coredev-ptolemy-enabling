import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLFloat,
    GraphQLBoolean
} from 'graphql';

import {
    ObjectType
} from '../../../../../../ObjectType';

import ImageValue from '../../ImageValue';

import Paragraph from '../../../../../../Paragraph';

const Video360Value = new GraphQLObjectType({
    name: 'Video360Value',
    fields: () => ({
        headline: {
            type: GraphQLString
        },
        siteKillDate: {
            type: GraphQLString
        },
        appearance: {
            type: GraphQLString
        },
        description: {
            type: new GraphQLList(Paragraph)
        },
        thumbnails: {
            type: new GraphQLList(ImageValue)
        },
        caption: {
            type: GraphQLString
        },
        type: {
            type: GraphQLString
        },
        lastModifiedUser: {
            type: GraphQLString
        },
        encodeJobId: {
            type: GraphQLInt
        },
        autoplay: {
            type: GraphQLBoolean
        },
        category: {
            type: GraphQLString
        },
        file: {
            type: GraphQLString
        },
        lastModifiedDate: {
            type: GraphQLString
        },
        dateCreated: {
            type: GraphQLString
        },
        slug: {
            type: GraphQLString
        },
        intlAutoplay: {
            type: GraphQLBoolean
        },
        trt: {
            type: GraphQLFloat
        },
        videoRate: {
            type: GraphQLString
        },
        cdnUrls: {
            type: ObjectType
        }
    })
});

export default Video360Value;
