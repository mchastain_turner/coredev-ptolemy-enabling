/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import {ObjectType} from '../../../../ObjectType';
import Reference from '../../../../Reference';

const InteractiveValue = new GraphQLObjectType({
    name: 'InteractiveValue',
    fields: () => ({
        id: { type: GraphQLString },
        referenceId: { type: GraphQLString },
        type: { type: GraphQLString },
        location: { type: GraphQLString },
        subtype: { type: GraphQLString },
        slug: { type: GraphQLString },
        attributes: { type: ObjectType },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default InteractiveValue;
