import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const Status = new GraphQLObjectType({
    name: 'Status',
    fields: () => ({
        state: { type: GraphQLString }
    })
});

export default Status;
