import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import Parent from './Subs/Parent';
import Contributor from './Subs/Contributor';
import Topic from './Subs/Topic';
import RelatedMedia from './RelatedMedia';
import Body from './Subs/Body';
import { ObjectType } from '../ObjectType';

const Article = new GraphQLObjectType({
    name: 'Article',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        title: { type: GraphQLString },
        mobileHeadline: { type: GraphQLString },
        socialHeadline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        body: {
            type: Body
        },
        source: { type: GraphQLString },
        byline: { type: GraphQLString },
        branding: { type: GraphQLString },
        storyType: { type: GraphQLString },
        section: { type: GraphQLString },
        location: { type: GraphQLString },
        highlights: { type: new GraphQLList(Paragraph) },
        partner: { type: GraphQLString },
        smsbody: { type: GraphQLString },
        contributors: {
            type: new GraphQLList(Contributor)
        },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        parent: {
            type: Parent
        },
        language: { type: GraphQLString }
    })
});

export default Article;
