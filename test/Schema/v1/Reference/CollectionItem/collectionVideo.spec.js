import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/CollectionItem/CollectionVideo';

import video from '../video.fixture.json';
import video360 from '../video360.fixture.json';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const logStub = stub();
const loadStub = stub();

loadStub.withArgs('video_test')
    .returns(video[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }

    },
    log: logStub
};

describe('CollectionItem CollectionVideo Schema Tests', function() {
    describe('Basic schema reference test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                collectionVideo: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'video_test',
                            referenceId: 'video_test',
                            type: 'reference',
                            referenceType: 'video'
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the video as a hydrated reference', async function() {
            const query = `
                query CollectionVideo {
                    collectionVideo {
                        id
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    collectionVideo: {
                        id: 'video_test',
                        type: 'reference',
                        referenceType: 'video',
                        reference: {
                            id: 'video_test',
                            type: 'video'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

    describe('Basic schema target test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                collectionVideo: {
                    type: new GraphQLList(TypeSchema),
                    resolve() {
                        return [
                            video360[0],
                            {
                                id: 'video_test',
                                referenceId: 'video_test',
                                type: 'reference',
                                referenceType: 'video'
                            }
                        ];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the video as a hydrated reference', async function() {
            const query = `
                query CollectionVideo {
                    collectionVideo {
                        id
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        target {
                            ...on Video{
                                id
                                type
                            }
                            ...on Video360Value{
                                headline
                                caption
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    collectionVideo: [{
                        id: null,
                        type: 'element',
                        referenceType: null,
                        reference: null,
                        target: {
                            headline: '360 video headline',
                            caption: '360 video caption'
                        }
                    },{
                        id: 'video_test',
                        type: 'reference',
                        referenceType: 'video',
                        reference: {
                            id: 'video_test',
                            type: 'video'
                        },
                        target: {
                            id: 'video_test',
                            type: 'video'
                        }
                    }]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
