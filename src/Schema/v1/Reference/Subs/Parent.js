import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const Parent = new GraphQLObjectType({
    name: 'Parent',
    fields: () => ({
        id: { type: GraphQLString }
    })
});

export default Parent;
