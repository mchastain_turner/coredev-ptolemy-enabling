import Hapi from 'hapi';
import good from 'good';
import {promisify} from 'bluebird';
import {getHost, getPort} from './config';
import HttpPlugin from './Plugins/HttpPlugin';
import SchemaHandler from './Plugins/SchemaHandler';
import StatsdPlugin from './Plugins/StatsdPlugin';
import HealthCheck from './Plugins/HealthCheck';
import {
    getHttpBasePath,
    getHttpHostName,
    getHttpPort,
    getHttpTimeout,
    getBearerToken,
    getMaxBatchSize,
    getLoaderCacheEnabled,
    getLoaderCacheTtl,
    getStatsdUrl,
    getEnvironment,
    getHostname,
    getProductName,
    getVersion,
    configRouteHandler
} from './config';

const debug = process.env.LOG_DEBUG === '1';
const logOps = process.env.LOG_OPS === '1';

const hostname = require('os').hostname();
/**
 * Configure and start the Hapi server
 */
export default function runServer() {
    try {
        const server = new Hapi.Server();
        const statsdUrl = getStatsdUrl();
        console.log(`STATSD client: ${statsdUrl.hostname}:${statsdUrl.port}`);

        // Make server methods promise friendly
        for (const method of ['register', 'start']) {
            server[method] = promisify(server[method], server);
        }

        server.connection({
            address: '0.0.0.0',
            host: getHost(),
            port: getPort(),
            routes: {
                log: true
            }
        });

        const logConfig = debug ? '*' : { exclude: 'debug' };
        const goodSqueezeArgs = {
            log: logConfig
        };

        if (logOps) {
            goodSqueezeArgs.ops = '*';
        }

        server.register({
            register: good,
            options: {
                ops: {
                    interval: 1000
                },
                reporters: {
                    console: [
                        {
                            module: 'good-squeeze',
                            name: 'Squeeze',
                            args: [goodSqueezeArgs]
                        },
                        {
                            module: 'good-console',
                            args: [{
                                color: false
                            }]
                        },
                        'stdout'
                    ]
                }
            }
        });

        server.register({
            register: StatsdPlugin,
            options: {
                hostname: statsdUrl.hostname,
                port: statsdUrl.port,
                prefix: 'coredev-ptolemy',
                tags: [
                    `host:${hostname}`,
                    'product:ptolemy',
                    `environment:${getEnvironment()}`,
                    'owner:coredev'
                ]
            }
        });

        server.register({
            register: HttpPlugin,
            options: {
                hostname: getHttpHostName(),
                port: getHttpPort(),
                basePath: getHttpBasePath(),
                timeout: getHttpTimeout(),
                maxBatchSize: getMaxBatchSize(),
                cacheEnabled: getLoaderCacheEnabled(),
                cacheTtl: getLoaderCacheTtl()
            }
        });

        server.register({
            register: HealthCheck,
            options: {
                hostname: getHostname(),
                productName: getProductName(),
                version: getVersion()
            }
        });

        server.register({
            register: SchemaHandler,
            options: {
                bearerToken: getBearerToken(),
                db: HttpPlugin.db
            }
        }, (err) => {
            if(err) {
                throw err;
            }
        });

        server.route({
            method: 'GET',
            path: '/_config',
            handler: configRouteHandler
        });

        server.start();

        server.log(['info'], 'Server started at ' + server.info.uri);
    } catch(e) {
        console.log(e);
    }
}
