
import {
    graphqlHapi,
    graphiqlHapi
} from 'graphql-server-hapi';
import Schema from '../../Schema';
import AuthBearer from 'hapi-auth-bearer-token';

const sampleQuery = `query Section {
    section(id:"cnn/homepage") {
        id
    }
}`;

const passHeader =
    '"Authorization": window.__TOKEN ? "Bearer " + window.__TOKEN : ""';

function injectToken(request, reply) {
    const { response } = request;
    const token =
        request.auth && request.auth.credentials ?
        request.auth.credentials.token : null;

    // noop on errors
    if (response.isBoom || !response.source || !token) {
        return reply.continue();
    }

    return reply(response.source.replace('</head>',
        `<script>window.__TOKEN = "${token}";</script></head>`
    ));
}

function register(server, options, next) {

    server.register(AuthBearer);

    server.auth.strategy('bearer', 'bearer-access-token', {
        allowQueryToken: false,
        validateFunc(token, callback) {
            return callback(
                null,
                (token === options.bearerToken),
                { token },
                { });
        }
    });

    server.register({
        register: graphqlHapi,
        options: {
            path: '/svc/ptolemy/v1/query',
            graphqlOptions: {
                schema: Schema.v1,
                rootValue: {
                    db: options.db,
                    log: server.log.bind(server)
                }
            },
            route: {
                auth: 'bearer'
            }
        }
    });

    server.register(require('hapi-auth-basic-key'));

    server.auth.strategy('simple', 'basic', {
        validateFunc(req, key, password, callback) {
            if ( key === options.bearerToken ) {
                return callback(null, true, { token: key });
            } else {
                return callback(null, false);
            }
        }
    });

    server.register({
        register: graphiqlHapi,
        options: {
            route: {
                auth: {
                    strategy: 'simple'
                },
                    ext: {
                        onPreResponse: {
                            method: injectToken
                        }
                    }
            },
            path: '/svc/ptolemy/v1',
            graphiqlOptions: {
                endpointURL: '/svc/ptolemy/v1/query',
                query: sampleQuery,
                passHeader
            }
       }
    });

    next();
}

register.attributes = {
    name: 'SchemaHandler'
};

const SchemaHandler = {register, injectToken};

export default SchemaHandler;
