import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLFloat
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import Parent from './Subs/Parent';
import Topic from './Subs/Topic';
import { ObjectType } from '../ObjectType';
import RelatedMedia from './RelatedMedia';

const Video = new GraphQLObjectType({
    name: 'Video',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        title: { type: GraphQLString },
        videoId: { type: GraphQLString },
        cvpXmlUrl: { type: GraphQLString },
        affiliate: { type: GraphQLString },
        showName: { type: GraphQLString },
        source: { type: GraphQLString },
        franchise: { type: GraphQLString },
        branding: { type: GraphQLString },
        section: { type: GraphQLString },
        categories: {
            type: new GraphQLList(GraphQLString)
        },
        trt: { type: GraphQLFloat },
        duration: { type: GraphQLString },
        cdnUrls: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve(parent, {sizes}) {
                if ( sizes ) {
                    const cdnUrls = {};
                    Object.keys(parent.cdnUrls).filter(function(el) {
                        if (sizes.indexOf(el) >= 0) {
                            cdnUrls[el] = parent.cdnUrls[el];
                        }
                    });
                    return cdnUrls;
                } else {
                    return parent.cdnUrls;
                }
            }
        },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        parent: {
            type: Parent
        },
        language: { type: GraphQLString }
    })
});

export default Video;
