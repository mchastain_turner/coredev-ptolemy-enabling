/**
 * Simple quick and dirty command line tool to generate a bearer token.
 *
 * Usage:
 *
 *    node makeBearerToken.js <mypayload> <myprivatekey>
 *
 * Example:
 *    node makeBearerToken.js coredev-ptolemy/local thisisasecretshhhhh
 */
const jwt = require('jsonwebtoken');

if ( process.argv.length < 4 ) {
    throw new Error('Please provide 2 arguments, payload and private key');
}

const token = jwt.sign(process.argv[2], process.argv[3]);

console.log(token);
