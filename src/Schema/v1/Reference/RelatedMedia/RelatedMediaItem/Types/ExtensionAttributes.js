/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import {ObjectType} from '../../../../ObjectType';
import Paragraph from '../../../../Paragraph';

import {
    resolvePropertiesFactory
} from '../../../resolves';

const resolveCaptions = resolvePropertiesFactory('captions');

const ExtensionAttributes = new GraphQLObjectType({
    name: 'ExtensionAttributes',
    fields: () => ({
        branding: { type: GraphQLString },
        brandingOn: { type: GraphQLBoolean },
        captions: {
            type: ObjectType,
            args: {
                properties: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCaptions
        },
        contributorProfiles: { type: GraphQLString },
        dateCreated: { type: GraphQLString },
        flag: { type: GraphQLString },
        flagColor: { type: GraphQLString },
        kicker: { type: new GraphQLList(Paragraph) },
        sponsorship: { type: GraphQLString }
    })
});

export default ExtensionAttributes;
