/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';

import {ObjectType} from '../../ObjectType';
import Paragraph from '../../Paragraph';

import {
    resolveCuts
} from '../resolves';

const CollectionSlide = new GraphQLObjectType({
    name: 'CollectionSlide',
    fields: () => ({
        type: { type: GraphQLString },
        caption: { type: new GraphQLList(Paragraph) },
        headline: { type: GraphQLString },
        source: { type: GraphQLString },
        credit: { type: GraphQLString },
        thumbText: { type: GraphQLString },
        format: { type: GraphQLString },
        image: {
            type: new GraphQLObjectType({
                name: 'SlideImage',
                fields: () => ({
                    url: { type: GraphQLString }
                })
            })
        },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        }
    })
});

export default CollectionSlide;
