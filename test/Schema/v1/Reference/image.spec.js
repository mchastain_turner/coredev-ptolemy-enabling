import chai from 'chai';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './image.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Image';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

describe('Didgeridoo Image Schema Tests', function() {
    describe('Basic Image schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                image: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an image', async function() {
            const query = `
                query Image {
                    image {
                        id
                        type
                    }
                }
            `;

            const expected = {
                image: {
                    id: 'image_test',
                    type: 'image'
                }
            };

            const result = await graphql(Schema, query);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an image', async function() {
            const query = `
                query Image {
                    image {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        dam_id
                        attributes
                        caption
                        photographer
                        credit
                        usage
                        source
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        cuts
                        parent {
                            id
                        }
                        language
                        rights {
                            policyid
                            policytype
                            permissions {
                                action
                                assignee
                                target
                            }
                            prohibitions {
                                action
                                assignee
                                target
                            }
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    image: fixture[0]
                }
            };

            const result = await graphql(Schema, query);
            result.should.to.deep.equal(expected);
        });


    });

});
