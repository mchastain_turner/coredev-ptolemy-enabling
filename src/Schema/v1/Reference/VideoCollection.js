import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../Paragraph';
import Status from '../Status';
import AutoZone from './Subs/AutoZone';
import Parent from './Subs/Parent';
import Topic from './Subs/Topic';
import { ObjectType } from '../ObjectType';
import CollectionVideo from './CollectionItem/CollectionVideo';

const VideoCollection = new GraphQLObjectType({
    name: 'VideoCollection',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        title: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        attributes: {
            type: ObjectType
        },
        collectionType: { type: GraphQLString },
        query: { type: GraphQLString },
        section: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        videos: {
            type: new GraphQLList(CollectionVideo)
        },
        distributionPlatforms: {
            type: new GraphQLList(GraphQLString)
        },
        parent: {
            type: Parent
        },
        autoZone: {
            type: AutoZone
        },
        language: { type: GraphQLString }
    })
});

export default VideoCollection;
