import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLList
} from 'graphql';

import Card from './Card';
import Links from './Links';
import { ObjectType } from './ObjectType';
import Status from './Status';

const Composite = new GraphQLObjectType({
    name: 'Composite',
    fields: () => ({
        cards: {
            type: new GraphQLList(Card)
        },
        zone: { type: ObjectType },
        firstPublishDate: { type: GraphQLString },
        id: { type: GraphQLString },
        language: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        links: {
            type: Links
        },
        properties: { type: GraphQLString },
        section: { type: GraphQLString },
        status: {
            type: Status
        },
        topics: {
            type: new GraphQLList(ObjectType)
        },
        type: { type: GraphQLString },
        url: { type: GraphQLString },
        branding: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        sectionDisplayName: { type: GraphQLString }
    })
});

export default Composite;
