import {
    GraphQLString,
    GraphQLObjectType
} from 'graphql';

import ValueTarget from '../ValueTarget';

const ValueElement = new GraphQLObjectType({
    name: 'ValueElement',
    fields: () => ({
        appearance: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: GraphQLString },
        caption: { type: GraphQLString },
        imageOid: { type: GraphQLString },
        targetLinkUri: { type: GraphQLString },
        targetLinkType: { type: GraphQLString },
        targetLinkOid: { type: GraphQLString },
        type: { type: GraphQLString },
        elementType: { type: GraphQLString },
        url: { type: GraphQLString },
        location: { type: GraphQLString },
        target: {
            type: ValueTarget,
            resolve(obj) {
                return obj.target.handle;
            }
        }
    })
});

export default ValueElement;
