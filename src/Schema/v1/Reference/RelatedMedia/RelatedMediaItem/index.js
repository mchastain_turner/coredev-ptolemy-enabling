import {
  GraphQLUnionType
} from 'graphql';

import ProfileReference from './Types/ProfileReference';
import VideoReference from './Types/VideoReference';
import VideoCollectionReference from './Types/VideoCollectionReference';
import GalleryReference from './Types/GalleryReference';
import ArticleReference from './Types/ArticleReference';
import CoverageContainerReference from './Types/CoverageContainerReference';
import HyperlinkReference from './Types/HyperlinkReference';
import InteractiveReference from './Types/InteractiveReference';
import SpecialReference from './Types/SpecialReference';
import ValueCard from './Types/ValueWrappers/ValueCard';
import ValueElement from './Types/ValueWrappers/ValueElement';
import ImageValue from './Types/ImageValue';
import InteractiveValue from './Types/InteractiveValue';
import CollectionReference from './Types/CollectionReference';
import RelatedMediaZoneContainer from
'../../../Reference/RelatedMedia/RelatedMediaZone/RelatedMediaZoneContainer';
import UnknownType from '../../UnknownType';

function getTypes() {
    return [
        ProfileReference,
        VideoReference,
        VideoCollectionReference,
        GalleryReference,
        ArticleReference,
        CoverageContainerReference,
        HyperlinkReference,
        InteractiveReference,
        SpecialReference,
        ImageValue,
        InteractiveValue,
        ValueCard,
        ValueElement,
        CollectionReference,
        RelatedMediaZoneContainer,
        UnknownType
    ];
}

const typeMap = {
    value: {
        value: ValueCard,
        element: ValueElement,
        image: ImageValue,
        interactive: InteractiveValue
    },
    reference: {
        profile: ProfileReference,
        video: VideoReference,
        videoCollection: VideoCollectionReference,
        gallery: GalleryReference,
        article: ArticleReference,
        coverageContainer: CoverageContainerReference,
        hyperlink: HyperlinkReference,
        interactive: InteractiveReference,
        collection: CollectionReference,
        special: SpecialReference
    },
    container: RelatedMediaZoneContainer
};


function resolveReference(obj, log) {
    if ( typeMap.reference[obj.referenceType] ) {
        return typeMap.reference[obj.referenceType];
    } else {
        log(['warn', 'uknownType'],
            'Unknown or unsuported \'RelatedMediaItem\' reference ' +
            `referenceId: "${obj.referenceId}", ` +
            `referenceType: "${obj.referenceType}"`
        );
        return UnknownType;
    }
}

const RelatedMediaItem = new GraphQLUnionType({
    name: 'RelatedMediaItem',
    types: getTypes,
    resolveType(obj, _, {rootValue: {log}}) {
        if ( obj.type === 'reference' ) {
            return resolveReference(obj, log);
        } else if ( obj.type === 'container' ) {
            return typeMap.container;
        }else if ( typeMap.value[obj.type] ) {
            return typeMap.value[obj.type];
        } else {
            log(['warn', 'uknownType'],
                'Unknown or unsuported \'RelatedMediaItem\' value ' +
                `id: "${obj.id}", ` +
                `type: "${obj.type}"`
            );
            return UnknownType;
        }
    }
});

export default RelatedMediaItem;
