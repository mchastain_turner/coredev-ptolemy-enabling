/* eslint max-len:0 */
/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLUnionType
} from 'graphql';

import {ObjectType} from '../../ObjectType';
import Paragraph from '../../Paragraph';
import Video from '../Video';
import Video360Value from '../RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/Video360Value.js';
import RefError from '../RefError';
import UnknownType from '../UnknownType';

import {
    resolveCuts
} from '../resolves';

const videoType = new GraphQLUnionType({
    name: 'CollectionVideoTarget',
    types: [
        Video360Value,
        Video,
        RefError,
        UnknownType
    ],
    resolveType(obj, _, {rootValue: {log}}) {
        switch(obj.type) {
            case 'video360' : return Video360Value;
            case 'video': return Video;
            case 'error': return RefError;
            default:
                log(['warn', 'uknownType'],
                        `CollectionVideo: Unknown or unsuported 'Reference' id: "${obj.id}", ` +
                        `type "${obj.type}"`
                   );
                return UnknownType;
        }
    }
});

const CollectionVideo = new GraphQLObjectType({
    name: 'CollectionVideo',
    fields: () => ({
        id: { type: GraphQLString },
        type: { type: GraphQLString },
        location: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        trt: { type: GraphQLInt },
        duration: { type: GraphQLString },
        url: { type: GraphQLString },
        cvpXmlUrl: { type: GraphQLString },
        autoplay: { type: GraphQLBoolean },
        referenceType: { type: GraphQLString },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        reference: {
            type: Video,
            deprecationReason: 'CollectionVideo.target replaces reference',
            resolve(parent,
                    args,
                    context,
                    {rootValue: {db}}
                   ) {
                if(parent.referenceType === 'video') {
                    return db.resolveRef(parent);
                }

                return null;
            }
        },
        target: {
            type: videoType,
            resolve(parent,
                    args,
                    context,
                    {rootValue: {db}}
                   ) {

                if (parent.referenceType === 'video') {
                    return db.resolveRef(parent);
                }

                return db.resolveHandle(parent, 'target', parent.referenceId);
            }
        },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        }
    })
});

export default CollectionVideo;
