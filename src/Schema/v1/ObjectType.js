import { GraphQLScalarType } from 'graphql/type';

export const ObjectType = new GraphQLScalarType({
    name: 'ObjectType',
    serialize: value => {
        return value;
    }
});
