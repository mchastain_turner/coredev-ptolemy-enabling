import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

const AutoZone = new GraphQLObjectType({
    name: 'AutoZone',
    fields: () => ({
        auto: { type: GraphQLBoolean },
        query: { type: GraphQLString },
        queryUri: { type: GraphQLString }
    })
});

export default AutoZone;
