#!/usr/bin/env bash

HOST=$1
BEARER=eyJhbGciOiJIUzI1NiJ9.Y29yZWRldi1wdG9sZW15L2xvY2Fs.ABdKjddj0G6gc6ruFtLv5Thnrys9IhSLOJyW6cadsnM

curl --insecure -vso /dev/null \
  -H"Authorization: bearer ${BEARER}" \
  ${HOST}/svc/ptolemy/v1/query -d@'./sample-queries/search.txt'

