/* eslint max-len:0 */
import {
    GraphQLObjectType,
    GraphQLUnionType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import {ObjectType} from '../../ObjectType';
import Paragraph from '../../Paragraph';

import Article from '../Article';
import Collection from '../Collection';
import CoverageContainer from '../CoverageContainer';
import Gallery from '../Gallery';
import Image from '../Image';
import Profile from '../Profile';
import Section from '../Section';
import Video from '../Video';
import VideoCollection from '../VideoCollection';
import UnknownType from '../UnknownType';

import
    EmbedValue
from '../RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/EmbedValue';
import
    Video360
from '../RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/Video360Value.js';

import {
    resolveCuts
} from '../resolves';

const CollectionItemReference = new GraphQLObjectType({
    name: 'CollectionItemReference',
    fields: () => ({
        id: { type: GraphQLString },
        type: {
            type: GraphQLString,
            resolve(parent) {
                return (
                    parent.type === 'element' ?
                    parent.elementType :
                    parent.type
                );
            }
        },
        location: { type: GraphQLString },
        url: { type: GraphQLString },
        headline: { type: GraphQLString },
        shortheadline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        referenceType: { type: GraphQLString },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        reference: {
            type: new GraphQLUnionType({
                name: 'CollectionItemReferenceItem',
                types: [
                    Article,
                    Collection,
                    CoverageContainer,
                    Gallery,
                    Image,
                    Profile,
                    Section,
                    Video,
                    VideoCollection,
                    EmbedValue,
                    Video360,
                    UnknownType
                ],
                resolveType(obj, _, {rootValue: {log}}) {
                    switch(obj.type) {
                        case 'article': return Article;
                        case 'collection': return Collection;
                        case 'coverageContainer': return CoverageContainer;
                        case 'gallery': return Gallery;
                        case 'image': return Image;
                        case 'profile': return Profile;
                        case 'section': return Section;
                        case 'video': return Video;
                        case 'video360': return Video360;
                        case 'videoCollection': return VideoCollection;
                        case 'embed': return EmbedValue;
                        default:
                            log(['warn', 'uknownType'],
                                `Unknown or unsuported 'CollectionItemReferenceItem' id: "${obj.id}", ` +
                                `type: "${obj.type}"`
                            );
                            return UnknownType;
                    }
                }
            }),
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'target', parent.id);
            }
        },
        sendToApps: { type: GraphQLBoolean },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        }
    })
});

export default CollectionItemReference;
