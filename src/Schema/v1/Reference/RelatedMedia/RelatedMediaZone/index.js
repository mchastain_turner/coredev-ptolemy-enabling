/* eslint max-len:0 */
import {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import RelatedMediaItem from '../RelatedMediaItem';
import Reference from '../../../Reference';
import {ObjectType} from '../../../ObjectType';

const RelatedMediaZone = new GraphQLObjectType({
    name: 'RelatedMediaZone',
    fields: () => ({
        limit: {
            description: 'The max number of cards supposed to be in this zone',
            type: GraphQLInt
        },
        zoneType: {
            description: 'Container or Zone/Card',
            type: GraphQLString
        },
        displayLabel: {
            description: 'Display label',
            type: GraphQLBoolean
        },
        label: {
            description: 'The text heading for this zone',
            type: GraphQLString
        },
        showAds: {
            description: 'Display ads',
            type: GraphQLBoolean
        },
        id: {
            description: 'Unique name for this zone',
            type: GraphQLString
        },
        priority: {
            type: GraphQLString
        },
        branding: {
            description: 'The branding to use on this zone',
            type: GraphQLString
        },
        options: {
            description: 'Data consumer added options',
            type: ObjectType
        },
        children: {
            description: 'Children content cards and content containers',
            type: new GraphQLList(RelatedMediaItem)
        },
        resource: {
            description: 'A media resource for the Zone, currently only images as background image',
            type: Reference,
            resolve(parent,
                    args,
                    context,
                    {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        },
        target: {
            type: Reference,
            description: 'The target attached to the zone, resolved to the type',
            resolve(parent,
                    args,
                    context,
                    {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'target');
            }
        }
    })
});

export default RelatedMediaZone;
