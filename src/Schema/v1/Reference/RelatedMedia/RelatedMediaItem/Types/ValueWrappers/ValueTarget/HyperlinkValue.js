import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

const HyperlinkValue = new GraphQLObjectType({
    name: 'HyperlinkValue',
    fields: () => ({
        type: {
            type: GraphQLString,
            description: 'Type of this object. Should always be "hyperlink"'
        },
        url: {
            type: GraphQLString,
            description: 'Url for this hyperlink'
        },
        text: {
            type: GraphQLString,
            description: 'Display text for this hyperlink'
        },
        newWindow: {
            type: GraphQLBoolean,
            description: 'Whether or not this should open in a new window'
        }
    })

});

export default HyperlinkValue;
