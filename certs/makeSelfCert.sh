#!/usr/bin/env bash

FQDN=$1
FQDN_UNDERED=$(sed 's/\./_/g' <<< $FQDN)

openssl req -x509 -days 3650 -newkey rsa:2048 -nodes \
    -keyout ${FQDN_UNDERED}.key \
    -out ${FQDN}.csr \
    -subj "/C=US/ST=GA/L=Atlanta/O=Turner Broadcasting System, Inc/OU=MSS/CN=${FQDN}"
