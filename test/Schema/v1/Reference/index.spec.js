import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import article from './article.fixture.json';
import video from './video.fixture.json';
import gallery from './gallery.fixture.json';
// TODO: This is not a complete model since there were no examples at the time
import videoCollection from './videoCollection.fixture.json';

import image from './image.fixture.json';
import section from './section.fixture.json';
import profile from './profile.fixture.json';
import coverageContainer from './coverageContainer.fixture.json';

import Reference from '../../../../src/Schema/v1/Reference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

/**
 * Homepage was pulled directly from the didgeridoo api and stored as a file,
 * which is used here as a fixture. Here we load it up as the 'db' to be
 * used to get dater from the fixture
 */

const getStub = stub();

getStub.withArgs('article').returns(article);
getStub.withArgs('video').returns(video);
getStub.withArgs('gallery').returns(gallery);
getStub.withArgs('videoCollection').returns(videoCollection);
getStub.withArgs('image').returns(image);
getStub.withArgs('section').returns(section);
getStub.withArgs('profile').returns(profile);
getStub.withArgs('coverageContainer').returns(coverageContainer);

const logStub = stub();

const qopts = {
    db: {
        get: getStub
    },
    log: logStub
};

describe('Didgeridoo Reference Query Tests', function() {
    describe('Basic Reference schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                reference: {
                    type: new GraphQLList(Reference),
                    args: {
                        id: {
                            type: new GraphQLNonNull(GraphQLID)
                        }
                    },
                    resolve({db}, {id}) {
                        return db.get(id);
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an article', async function() {
            const query = `
                query Reference {
                    reference(id:"article") {
                        ...on Article {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'article_test',
                        type: 'article'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a video', async function() {
            const query = `
                query Reference {
                    reference(id:"video") {
                        ...on Video {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'video_test',
                        type: 'video'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a Gallery', async function() {
            const query = `
                query Reference {
                    reference(id:"gallery") {
                        ...on Gallery {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'gallery_test',
                        type: 'gallery'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a VideoCollection', async function() {
            const query = `
                query Reference {
                    reference(id:"videoCollection") {
                        ...on VideoCollection {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'videoCollection_test',
                        type: 'videoCollection'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a image', async function() {
            const query = `
                query Reference {
                    reference(id:"image") {
                        ...on Image {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'image_test',
                        type: 'image'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a section', async function() {
            const query = `
                query Reference {
                    reference(id:"section") {
                        ...on Section {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'section_test',
                        type: 'section'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a profile', async function() {
            const query = `
                query Reference {
                    reference(id:"profile") {
                        ...on Profile {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'profile_test',
                        type: 'profile'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a field on a coverageContainer', async function() {
            const query = `
                query Reference {
                    reference(id:"coverageContainer") {
                        ...on CoverageContainer {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                reference: [
                    {
                        id: 'coverageContainer_test',
                        type: 'coverageContainer'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('Error conditions', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                reference: {
                    type: new GraphQLList(Reference),
                    args: {
                        id: {
                            type: new GraphQLNonNull(GraphQLID)
                        }
                    },
                    resolve() {
                        return [
                            {
                                id: 'foobar_test',
                                type: 'foo'
                            }
                        ];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('return an error for unknown reference types', async function() {
            const query = `
                query Reference {
                    reference(id:"coverageContainer") {
                        ...on UnknownType {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    reference: [
                        {
                            id: 'foobar_test',
                            type: 'foo'
                        }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( expected );
        });
    });

});
