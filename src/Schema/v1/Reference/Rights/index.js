import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';

import RightsAction from './RightsAction';

const Rights = new GraphQLObjectType({
    name: 'Rights',
    fields: () => ({
        policyid: { type: GraphQLString },
        policytype: { type: GraphQLString },
        permissions: {
            type: new GraphQLList(RightsAction)
        },
        prohibitions: {
            type: new GraphQLList(RightsAction)
        }
    })
});

export default Rights;
