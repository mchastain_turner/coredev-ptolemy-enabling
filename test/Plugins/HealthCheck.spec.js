import chai from 'chai';
import {
    stub
} from 'sinon';
import HealthCheck from '../../src/Plugins/HealthCheck';

chai.should();

describe('HealthCheck', function() {
    it('should call the response with a successful health check', function(done) {
        let routeOptions;

        const stubServer = {
            route(options) {
                routeOptions = options;
            }
        };

        HealthCheck.register(stubServer, {
                hostname: 'testhost',
                productName: 'testname',
                version: '9.9.9-99'
            },
            () => {
                const resStub = stub();

                routeOptions.should.have.all.keys(
                    'method',
                    'path',
                    'handler'
                );

                routeOptions.handler.should.be.a('function');

                routeOptions.handler(null, resStub);

                resStub.should.have.been.calledOnce
                    .and.calledWith({
                        status: 'UP',
                        hostname: 'testhost',
                        productName: 'testname',
                        version: '9.9.9-99'
                    });

                done();
            }
        );


    });
});
