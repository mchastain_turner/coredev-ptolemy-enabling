import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/CollectionReference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import collection from '../collection.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('collection_test')
    .returns(collection[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }
    },
    log: logStub
};

describe('RelatedMediaItem CollectionReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                collectionReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'collection_test',
                            referenceId: 'collection_test',
                            type: 'reference',
                            referenceType: 'collection',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the collection as a hydrated reference', async function() {
            const query = `
                query CollectionReference {
                    collectionReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    collectionReference: {
                        referenceId: 'collection_test',
                        type: 'reference',
                        referenceType: 'collection',
                        reference: {
                            id: 'collection_test',
                            type: 'collection'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
