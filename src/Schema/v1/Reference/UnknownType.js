import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const UnknownType = new GraphQLObjectType({
    name: 'UnknownType',
    fields: () => ({
        id: { type: GraphQLString },
        type: { type: GraphQLString },
        referenceId: { type: GraphQLString },
        referenceType: { type: GraphQLString }
    })
});

export default UnknownType;
