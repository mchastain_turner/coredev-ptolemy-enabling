import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import vcFixture from './videoCollection.fixture.json';
import vcFixtureAutoZone from './videoCollection-autoZone.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/VideoCollection';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

let fixture;

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub
    }
};

describe('Didgeridoo VideoCollection Schema Tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            videoCollection: {
                type: TypeSchema,
                resolve() {
                    return fixture;
                }
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    describe('Basic VideoCollection schema test', function() {
        it('should return a field on an videoCollection', async function() {
            fixture = vcFixture[0];
            const query = `
                query VideoCollection {
                    videoCollection {
                        id
                        type
                    }
                }
            `;

            const expected = {
                videoCollection: {
                    id: 'videoCollection_test',
                    type: 'videoCollection'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an videoCollection', async function() {
            fixture = vcFixture[0];
            const query = `
                query VideoCollection {
                    videoCollection {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        title
                        description {
                            id
                            plaintext
                            richtext
                            elements {
                                ...on Handle {
                                    type
                                    attributes
                                    target {
                                        type
                                        subtype
                                        referenceUrl
                                        referenceUri
                                    }
                                }
                                ...on Embed {
                                    type
                                    attributes
                                }
                            }
                            format
                        }
                        attributes
                        collectionType
                        query
                        section
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        videos {
                            id
                            type
                            location
                            headline
                            description {
                                id
                                plaintext
                                richtext
                                elements {
                                    ...on Handle {
                                        type
                                        attributes
                                        target {
                                            type
                                            subtype
                                            referenceUrl
                                            referenceUri
                                        }
                                    }
                                    ...on Embed {
                                        type
                                        attributes
                                    }
                                }
                                format
                            }
                            trt
                            duration
                            url
                            cvpXmlUrl
                            autoplay
                            referenceType
                            referenceUrl
                            referenceUri
                            cuts
                        }
                        distributionPlatforms
                        parent { id }
                        autoZone { auto }
                        language
                    }
                }
            `;
            const expected = {
                data: {
                    videoCollection: fixture
                }
            };

            const actual = await graphql(Schema, query, qopts);
            actual.should.to.deep.equal(expected);
        });
    });

    describe('VideoCollection with autozone schema test', function() {

        it('should return all fields on an videoCollection with autozone enabled', async function() {
            fixture = vcFixtureAutoZone[0];

            const query = `
                query VideoCollection {
                    videoCollection {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        title
                        description {
                            id
                            plaintext
                            richtext
                            elements {
                                ...on Handle {
                                    type
                                    attributes
                                    target {
                                        type
                                        subtype
                                        referenceUrl
                                        referenceUri
                                    }
                                }
                                ...on Embed {
                                    type
                                    attributes
                                }
                            }
                        }
                        attributes
                        collectionType
                        query
                        section
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        videos {
                            id
                            type
                            location
                            headline
                            description {
                                id
                                plaintext
                                richtext
                                elements {
                                    ...on Handle {
                                        type
                                        attributes
                                        target {
                                            type
                                            subtype
                                            referenceUrl
                                            referenceUri
                                        }
                                    }
                                    ...on Embed {
                                        type
                                        attributes
                                    }
                                }
                                format
                            }
                            trt
                            duration
                            url
                            cvpXmlUrl
                            autoplay
                            referenceType
                            referenceUrl
                            referenceUri
                            cuts
                        }
                        distributionPlatforms
                        parent { id }
                        autoZone {
                            auto
                            query
                            queryUri
                        }
                        language
                    }
                }
            `;
            const expected = {
                data: {
                    videoCollection: fixture
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

    describe('VideoCollection format schema test', function() {

        it('should return null for format on a videoCollection with no format field', async function() {
            fixture = {
                description: [
                    {
                        id: 'foo',
                        plaintext: 'test plain text',
                        richtext: null,
                        elements: []
                    }
                ]
            };

            const query = `
                query VideoCollection {
                    videoCollection {
                        description {
                            id
                            format
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    videoCollection: {
                        description: [
                            {
                                id: 'foo',
                                format: null
                            }
                        ]
                    }
                }
            };


            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return format on a videoCollection with a format field', async function() {
            fixture = {
                description: [
                    {
                        id: 'foo',
                        plaintext: 'test plain text',
                        richtext: null,
                        elements: [],
                        format: []
                    }
                ]
            };

            const query = `
                query VideoCollection {
                    videoCollection {
                        description {
                            id
                            format
                        }
                    }
                }
            `;
            const expected = {
                data: {
                    videoCollection: {
                        description: [
                            {
                                id: 'foo',
                                format: []
                            }
                        ]
                    }
                }
            };


            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });
    });

});
