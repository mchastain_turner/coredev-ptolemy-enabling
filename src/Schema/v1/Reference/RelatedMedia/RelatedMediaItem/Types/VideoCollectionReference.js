/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import {ObjectType} from '../../../../ObjectType';
import Paragraph from '../../../../Paragraph';
import Reference from '../../../../Reference';

import {
    resolveCuts
} from '../../../resolves';
import VideoCollection from '../../../VideoCollection';
import ExtensionAttributes from './ExtensionAttributes';

const VideoCollectionReference = new GraphQLObjectType({
    name: 'VideoCollectionReference',
    fields: () => ({
        id: { type: GraphQLString },
        referenceId: { type: GraphQLString },
        type: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        banner: { type: new GraphQLList(Paragraph) },
        referenceType: { type: GraphQLString },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        reference: {
            type: VideoCollection,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveRef(parent);
            }
        },
        location: { type: GraphQLString },
        url: { type: GraphQLString },
        sendToApps: { type: GraphQLBoolean },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        auxiliaryText: { type: GraphQLString },
        extensionAttributes: { type: ExtensionAttributes },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default VideoCollectionReference;
