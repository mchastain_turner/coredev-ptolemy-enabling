import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';

const AnimationValue = new GraphQLObjectType({
    name: 'AnimationValue',
    fields: () => ({
        tags: {
            type: new GraphQLList(GraphQLString)
        },
        caption: {
            type: GraphQLString
        },
        appearance: {
            type: GraphQLString
        },
        type: {
            type: GraphQLString
        },
        mediaType: {
            type: GraphQLString
        },
        uri: {
            type: GraphQLString
        },
        url: {
            type: GraphQLString
        },
        filename: {
            type: GraphQLString
        },
        targetLinkType: {
            type: GraphQLString
        },
        targetLinkUri: {
            type: GraphQLString
        },
        lastModifiedUser: {
            type: GraphQLString
        },
        lastModifiedDate: {
            type: GraphQLString
        },
        dateCreated: {
            type: GraphQLString
        },
        slug: {
            type: GraphQLString
        },
        altTag: {
            type: GraphQLString
        }
    })
});

export default AnimationValue;
