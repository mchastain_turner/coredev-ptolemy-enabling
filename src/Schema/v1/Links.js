import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const Links = new GraphQLObjectType({
    name: 'Links',
    fields: () => ({
        next: { type: GraphQLString },
        self: { type: GraphQLString },
        prev: { type: GraphQLString }
    })
});

export default Links;
