import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Status from '../Status';
import Topic from './Subs/Topic';
import RelatedMedia from './RelatedMedia';
import { ObjectType } from '../ObjectType';

const Special = new GraphQLObjectType({
    name: 'Special',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        attributes: {
            type: ObjectType
        },
        branding: { type: GraphQLString },
        section: { type: GraphQLString },
        topicHead: { type: GraphQLString },
        topicIntro: { type: GraphQLString },
        title: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        language: { type: GraphQLString }
    })
});

export default Special;
