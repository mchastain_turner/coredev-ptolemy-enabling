require('babel-polyfill');

export function cleanTarget(target) {
    const hiddenVals = ['_id','_rev','notes','numericId','schemaVersion'];
    hiddenVals.forEach((v) => {
        delete target[v];
    });
}
