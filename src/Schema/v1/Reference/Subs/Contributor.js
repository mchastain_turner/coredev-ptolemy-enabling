import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const Contributor = new GraphQLObjectType({
    name: 'Contributor',
    fields: () => ({
        firstName: { type: GraphQLString },
        middleName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        fullName: { type: GraphQLString }
    })
});

export default Contributor;
