/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Parent from '../Paragraph';
import Status from '../Status';
import Topic from './Subs/Topic';
import Rights from './Rights';
import { ObjectType } from '../ObjectType';
import { resolveCuts } from './resolves';

const Image = new GraphQLObjectType({
    name: 'Image',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        dam_id: { type: GraphQLString },
        attributes: {
            type: ObjectType
        },
        caption: { type: GraphQLString },
        photographer: { type: GraphQLString },
        credit: { type: GraphQLString },
        usage: { type: GraphQLString },
        source: { type: GraphQLString },
        topics: {
            type: new GraphQLList(Topic)
        },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        parent: {
            type: Parent
        },
        language: { type: GraphQLString },
        rights: {
            type: new GraphQLList(Rights)
        }
    })
});

export default Image;
