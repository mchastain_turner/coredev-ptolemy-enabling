import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './article.fixture.json';
import expectedFixture from './article.expected.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Article';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const logStub = stub();

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDocs: getStub
    },
    log: logStub
};


describe('Didgeridoo Article Schema Tests', function() {
    describe('Basic Article schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                article: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an article', async function() {
            const query = `
                query Article {
                    article {
                        id
                        type
                    }
                }
            `;

            const expected = {
                article: {
                    id: 'article_test',
                    type: 'article'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.deep.equal( { data: expected });
        });

        it('should return all fields on an article using fragments', async function() {
            const query = `
                query Article {
                    article {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        title
                        mobileHeadline
                        socialHeadline
                        description {
                            ...ParagraphFragment
                        }
                        attributes
                        body {
                            footer {
                                type
                                text
                                paragraphs {
                                    ...ParagraphFragment
                                }
                            }
                            notes {
                                www
                                intl
                                wwwText {
                                    ...ParagraphFragment
                                }
                                intlText {
                                    ...ParagraphFragment
                                }
                                type
                            }
                            paragraphs {
                                ...ParagraphFragment
                            }
                            wordCount
                        }
                        source
                        byline
                        branding
                        storyType
                        section
                        location
                        highlights {
                            ...ParagraphFragment
                        }
                        partner
                        smsbody
                        contributors {
                            firstName
                            middleName
                            lastName
                            fullName
                        }
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideo360
                            hasVideoCollection
                            hasInteractive
                            media {
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFragment
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    type
                                    headline
                                    description {
                                        ...ParagraphFragment
                                    }
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                }
                                ...on ImageValue {
                                    ...ImageValueFragment
                                }
                                ...on InteractiveValue {
                                    id
                                    referenceId
                                    type
                                    location
                                    subtype
                                    slug
                                    attributes
                                }
                                ...on GalleryReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on ArticleReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                                ...on ValueCard {
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    banner {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                        format
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                    target{
                                        ...on EmbedValue{
                                            dateCreated
                                            lastModifiedDate
                                            lastModifiedUser
                                            mediaType
                                            oembed
                                            slug
                                            type
                                            url
                                        }
                                  }
                                  extensionAttributes{
                                        branding
                                        captions
                                        dateCreated
                                        flag
                                        flagColor
                                        kicker{
                                            id
                                            plaintext
                                            richtext
                                            elements {
                                                ...on Handle {
                                                    type
                                                    attributes
                                                    target {
                                                        type
                                                        subtype
                                                        referenceUrl
                                                        referenceUri
                                                    }
                                                }
                                                ...on Embed {
                                                    type
                                                    attributes
                                                }
                                            }
                                            format
                                        }
                                    }
                                 valueType
                                }
                                ...on ValueElement {
                                    appearance
                                    valueDescription : description
                                    headline
                                    caption
                                    imageOid
                                    targetLinkUri
                                    targetLinkType
                                    targetLinkOid
                                    type
                                    elementType
                                    url
                                    location
                                    target{
                                        ...on AnimationValue{
                                            tags
                                            caption
                                            type
                                            mediaType
                                            uri
                                            url
                                            filename
                                            lastModifiedUser
                                            lastModifiedDate
                                            dateCreated
                                            slug
                                            altTag
                                            appearance
                                            targetLinkUri
                                            targetLinkType
                                        }
                                        ...on MapValue{
                                            tags
                                            caption
                                            type
                                            lastModifiedUser
                                            lastModifiedDate
                                            dateCreated
                                            images
                                            mediaType
                                            name
                                            slug
                                            data
                                        }
                                        ...on Video360Value {
                                            headline
                                            siteKillDate
                                            appearance
                                            description {
                                                ...ParagraphFormatFragment
                                            }
                                            thumbnails {
                                                ...ImageValueFragment
                                            }
                                            caption
                                            encodeJobId
                                            type
                                            lastModifiedUser
                                            autoplay
                                            category
                                            file
                                            lastModifiedDate
                                            dateCreated
                                            slug
                                            intlAutoplay
                                            trt
                                            videoRate
                                            cdnUrls
                                        }
                                    }
                                }
                            }
                            autoZone {
                                auto
                            }
                        }
                        parent { id }
                        language
                    }
                }
                fragment ParagraphFragment on Paragraph {
                    id
                    plaintext
                    richtext
                    elements {
                        ...on Handle {
                            type
                            attributes
                            target {
                                type
                                subtype
                                referenceUrl
                                referenceUri
                            }
                        }
                        ...on Embed {
                            type
                            attributes
                        }
                    }
                }
                fragment ParagraphFormatFragment on Paragraph {
                    ...ParagraphFragment
                    format
                }
                fragment ImageValueFragment on ImageValue {
                    id
                    imageId
                    type
                    referenceUrl
                    referenceUri
                    location
                    slug
                    dam_id
                    photographer
                    caption
                    cuts
                }
            `;
            const expected = {
                data: {
                    article: expectedFixture
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });


    });

});
