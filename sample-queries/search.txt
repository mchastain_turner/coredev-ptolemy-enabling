query=query Search {
    search(query:"type:article/rows:10") {
        status
        copyright
        results
        links {
            nextUri
            prevUri
            selfUri
        }
        start
        docs {
            ...on Article{
                id
                sourceId
                type
                dataSource
                schema
                schemaVersion
                status {state}
                firstPublishDate
                lastPublishDate
                lastModifiedDate
                slug
                url
                headline
                title
                mobileHeadline
                socialHeadline
                description {
                    ...ParagraphFragment
                }
                source
                byline
                branding
                storyType
                section
                location
                highlights {
                    ...ParagraphFragment
                }
                partner
                smsbody
                contributors {
                    firstName
                    middleName
                    lastName
                    fullName
                }
                topics {
                    ...TopicsFragment
                }
                relatedMedia {
                    ...RelatedMediaFragment
                }
                parent { id }
                language
            }
            ...on Gallery{
                id
                sourceId
                type
                dataSource
                schema
                schemaVersion
                status {state}
                firstPublishDate
                lastPublishDate
                lastModifiedDate
                slug
                url
                headline
                title
                description {
                    ...ParagraphFragment
                }
                attributes
                source
                branding
                section
                topics {
                    ...TopicsFragment
                }
                slides {
                    type
                    caption {
                        ...ParagraphFragment
                    }
                    headline
                    source
                    credit
                    thumbText
                    format
                    image {
                        url
                    }
                }
                relatedMedia {
                    ...RelatedMediaFragment
                }
                parent { id }
                language
            }
            ...on Video{
                id
                sourceId
                type
                dataSource
                schema
                schemaVersion
                status {state}
                firstPublishDate
                lastPublishDate
                lastModifiedDate
                slug
                url
                headline
                description {
                    ...ParagraphFragment
                }
                attributes
                title
                videoId
                cvpXmlUrl
                affiliate
                showName
                source
                franchise
                branding
                section
                categories
                trt
                duration
                cdnUrls
                topics {
                    ...TopicsFragment
                }
                relatedMedia {
                    ...RelatedMediaFragment
                }
                parent { id }
                language
            }
            ...on VideoCollection{
                id
                sourceId
                type
                dataSource
                schema
                schemaVersion
                status {state}
                firstPublishDate
                lastPublishDate
                lastModifiedDate
                slug
                url
                headline
                title
                description {
                    ...ParagraphFragment
                }
                attributes
                collectionType
                query
                section
                topics {
                    ...TopicsFragment
                }
                videos {
                    id
                    type
                    location
                    headline
                    trt
                    duration
                    url
                    cvpXmlUrl
                    autoplay
                    referenceType
                    referenceUrl
                    referenceUri
                    reference {
                        id
                        type
                    }
                }
                distributionPlatforms
                parent { id }
                autoZone {
                    auto
                    query
                    queryUri
                }
                language
            }
            ...on Collection {
                id
                sourceId
                type
                dataSource
                schema
                schemaVersion
                status {state}
                firstPublishDate
                lastPublishDate
                lastModifiedDate
                slug
                url
                headline
                title
                description {
                    ...ParagraphFragment
                }
                attributes
                section
                sponsorship
                topics {
                    ...TopicsFragment
                }
                content {
                    id
                    type
                    location
                    headline
                    shortheadline
                    referenceType
                    referenceUrl
                    reference {
                        ...on Article {
                            id
                            type
                        }
                        ...on Video {
                            id
                            type
                        }
                    }
                    referenceUri
                }
                autoZone { auto }
                language
            }
        }
    }
}
fragment ParagraphFragment on Paragraph {
    ...ParagraphNoFormatFragment
    format
}

fragment ParagraphNoFormatFragment on Paragraph {
    id
    plaintext
    richtext
    elements {
        ...on Handle {
            type
            attributes
            target {
                type
                subtype
                referenceUrl
                referenceUri
            }
        }
        ...on Embed {
            type
            attributes
        }
    }
}

fragment RelatedMediaFragment on RelatedMedia {
    hasImage
    hasGallery
    hasVideo
    hasVideoCollection
    hasInteractive
    media {
        ...on VideoReference {
            id
            referenceId
            type
            location
            headline
            description {
                ...ParagraphNoFormatFragment
            }
            url
            cvpXmlUrl
            autoplay
            duration
            reference {
                id
                type
            }
            referenceType
            referenceUrl
            referenceUri
            auxiliaryText
        }
        ...on VideoCollectionReference {
            id
            referenceId
            type
            headline
            description {
                ...ParagraphFragment
            }
            reference {
                id
                type
            }
            referenceType
            referenceUrl
            referenceUri
            location
            url
        }
        ...on ImageValue {
            id
            imageId
            type
            referenceUrl
            referenceUri
            location
            slug
            dam_id
            photographer
            caption
        }
        ...on InteractiveValue {
            id
            referenceId
            type
            location
            subtype
            slug
            attributes
        }
        ...on GalleryReference {
            id
            referenceId
            type
            location
            headline
            description {
                ...ParagraphFragment
            }
            banner {
                ...ParagraphFragment
            }
            url
            reference {
                id
                type
            }
            referenceType
            referenceUrl
            referenceUri
            sendToApps
            auxiliaryText
        }
        ...on ArticleReference {
            id
            referenceId
            type
            location
            headline
            description {
                ...ParagraphFragment
            }
            banner {
                ...ParagraphFragment
            }
            url
            reference {
                id
                type
            }
            referenceType
            referenceUrl
            referenceUri
            sendToApps
            auxiliaryText
        }
        ...on ProfileReference {
            id
            referenceId
            type
            location
            headline
            description {
                ...ParagraphFragment
            }
            banner {
                ...ParagraphFragment
            }
            url
            reference {
                id
                type
            }
            referenceType
            referenceUrl
            referenceUri
            sendToApps
            auxiliaryText
        }
    }
    autoZone {
        auto
    }
}
fragment TopicsFragment on Topic {
    label
    class
    id
    topicID
    confidenceScore
    leafNode
    parentPathIds
}
