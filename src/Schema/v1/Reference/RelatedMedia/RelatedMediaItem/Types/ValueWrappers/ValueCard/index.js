import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean,
    GraphQLEnumType
} from 'graphql';

import {ObjectType} from '../../../../../../ObjectType';
import Paragraph from '../../../../../../Paragraph';
import ValueTarget from '../ValueTarget';
import Reference from '../../../../../../Reference';

import {
    resolveCuts
} from '../../../../../resolves.js';

import ExtensionAttributes from '../../ExtensionAttributes';


const ValueCard = new GraphQLObjectType({
    name: 'ValueCard',
    fields: () => ({
        id: { type: GraphQLString },
        referenceId: { type: GraphQLString },
        type: { type: GraphQLString },
        location: { type: GraphQLString },
        headline: { type: GraphQLString },
        description: { type: new GraphQLList(Paragraph) },
        banner: { type: new GraphQLList(Paragraph) },
        url: { type: GraphQLString },
        referenceType: { type: GraphQLString },
        valueType: { type: new GraphQLEnumType({
            name: 'valueCardTypes',
            values: {
                embed: {
                    value: 'embed'
                },
                animation: {
                    value: 'animation'
                },
                liveVideo: {
                    value: 'liveVideo'
                },
                video360: {
                    value: 'video360'
                }
            }
        })
        },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        sendToApps: { type: GraphQLBoolean },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        target: {
            type: ValueTarget,
            resolve(obj) {
                return obj.target.handle;
            }
        },
        auxiliaryText: { type: GraphQLString },
        extensionAttributes: {
            type: ExtensionAttributes
        },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default ValueCard;
