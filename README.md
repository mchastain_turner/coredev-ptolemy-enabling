# Ptolemy

Named after [Ptolemy](https://en.wikipedia.org/wiki/Ptolemy), who was an early celestial cartographer.

Not much yet, but more to come. It's a start.

This project started from [GraphQL nodejs example](https://github.com/reindexio/graphql-nodejs-newsfeed)

Look in the .nvmrc and/or package.json for the preferred version of nodejs engine to use.

_note: if you are using [nvm](https://github.com/creationix/nvm) it should try to use the correct version of node_

To install, run `make install`

To run: `make start`

To run with inspection/debugging turned on run: `make inspect=true start`

Or you can use docker:

```
make dockerbuild
make dockerrun
```

## Log Levels

There are two **optional** environment variables to increase logging:
:w

**LOG\_DEBUG=1** - Allows messages tagged with `debug` to get logged
**LOG\_OPS=1** - Turns on `ops` logging (memory, uptime, load) *VERY CHATY!*

## API

The API is to POST your query to `/svc/ptolemy/{version}/query`
Version will control which version of the schema to use for your query. Currently the only version supported is __v1__. The only content type currently supported is `application/x-www-form-urlencoded`.

_There may be future plans to support_ `application/graphql`

This means that your POST body needs to be a www form style post containing the [GraphQL query](http://graphql.org/docs/queries/), like so:

```
query=my query
```
### GraphiQL (it's like swagger for your graphql)

To use graphiql, just navigate to the `/svc/ptolemy/v1` for the graphql host. [For example, here is graphiql link for production](https://coredev-capi-ptolemy.prod.services.ec2.dmtio.net/svc/ptolemy/v1)

Paste the bearer token for production into the username field. Leave the password blank. You should be able to execute any query you like against production data, as well as see the documentation on the current schema.

### Authentication (Bearer Tokens)

Ptolemy is using bearer tokens to authenticate and authorize users. This is only on the `/svc/ptolemy/{version}/query` route. Please obtain a bearer token for the environment you are querying. Ptolemy only supports bearer tokens in the header, it does not support them on the query string.

Once you have a bearer token, set the Authorization header like so: `Authorization: bearer <bearer token goes here>`

For example:

```
curl -H'Authorization: bearer 123456789' http://localhost:5000/svc/ptolemy/v1/query -d@'./sample-queries/allfields-frags.txt'
```

### Using fragments

If you are using fragments for repeated blocks of your query, you can add them like so:

```
query=query Section{
    section(id:"cnn/homepage" start:30 rows:30) {
        cards{
            description {
                ...ParagraphFragment
            }
            headline
            banner {
                ...ParagraphFragment
            }
            wordCount
        }
    }
}
fragments ParagraphFragment on Paragraph {
    plaintext
    richtext
    elements {
        ...on Handle {
            type
            attributes
            target {
                type
                subtype
                referenceUrl
                referenceUri
            }
        }
        ...on Embed {
            type
            attributes
        }
    }
}
```

### Section query

To get a whole section from didgeridoo, such as [cnn/homepage](http://compositor.api.cnn.com/svc/mcs/v3/composites/sections/cnn/homepage), Query a section passing in the id, including the property. For example:

```
query=query Section{
    section(id:"cnn/homepage" start:10 rows:5) {
        cards{
            target {
                ...HandleFragment
            }
            resource {
                ...HandleFragment
            }
        }
    }
}
fragment HandleFragment on Reference{
    ...on RefError {
        id
        type
        error
    }
    ...on AnimationValue {
        appearance
        type
        mediaType
        url
        filename
        slug
    }
    ...on EmbedValue {
        tags
        oembed
        type
        mediaType
        url
        slug
    }
    ...on Article {
        id
        type
    }
    ...on Video {
        id
        type
    }
}
```

### Reference query (docs)

To query a single document by id, use the "doc" query, passing in the hypatia ID of the document. The schema for each of these docs is exactly the same as it is for the card references above. You do need to specify what fields you want for each type of document that could be returned for that ID. For example:

```
query=query Reference {
    doc(id:"h_1070a24ab8f0bf9c905af650a7821a36") {
        ...on Article{
            id
            type
        }
        ...on Gallery{
            id
            type
        }
        ...on Video{
            id
            type
        }
        ...on VideoCollection{
            id
            type
        }
        ...on Collection {
            id
            type
        }
    }
}
```

### RelatedMediaItem references

To hydrate a relatedMedia item's reference, just query for the `reference` field on a relatedMedia Item. It will get the referenced content item using referenceId on the reference item.

Supported types are:
- ArticleReference
- CoverageContainerReference
- GalleryReference
- ProfileReference
- VideoCollectionReference
- VideoReference

For example:

```
query=query Section{
    section(id:"cnn/homepage" start:10 rows:5) {
        cards{
            target {
                ...on Article{
                    type
                    relatedMedia {
                        ...on ArticleReference {
                            referenceId
                            referenceType
                            reference {
                                id
                                type
                            }
                        }
                        ...on CoverageContainerReference {
                            referenceId
                            referenceType
                            reference {
                                id
                                type
                            }
                        }
                    }
                }
            }
        }
    }
}
```

### Collection and VideoCollection references

To hydrate a collection (or video collection) item's reference, just query for the `reference` field on a content or videos Item. It will get the referenced content item using id on the reference item.

This example shows getting the reference from a Collection's content items. They can be any type, so you'll need to provide inline fragments for each type you care about. This example also shows that you can keep drilling down on collections, assuming this is even a valid configuration for them.

```
query=query Section{
    section(id:"cnn/homepage" start:10 rows:5) {
        cards{
            target {
                ...on Collection{
                    id
                    type
                    content {
                        id
                        type
                        referenceType
                        reference {
                            ...on Article {
                                id
                                type
                            }
                            ...on Collection {
                                id
                                type
                                reference {
                                    ...on Article {
                                        id
                                        type
                                    }
                                }
                            }
                            ...on CoverageContainer {
                                id
                                type
                            }
                            ...on Gallery {
                                id
                                type
                            }
                            ...on Image {
                                id
                                type
                            }
                            ...on Profile {
                                id
                                type
                            }
                            ...on Section {
                                id
                                type
                            }
                            ...on Video {
                                id
                                type
                            }
                            ...on VideoCollection {
                                id
                                type
                            }
                        }
                    }
                }
            }
        }
    }
}
```
This example shows a video collection. It is simpler because there will only ever be Video objects in the collection.

```
query=query Section{
    section(id:"cnn/homepage" start:10 rows:5) {
        cards{
            target {
                ...on VideoCollection{
                    id
                    type
                    content {
                        id
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                    }
                }
            }
        }
    }
}
```


## Example queries

To test, you can run a query like this:

```
curl http://localhost:5000/svc/ptolemy/v1/query --data 'query={section(id:"homepage") {type cards{headline, cuts} }}'
```

There are also ways to use introspection on the schemas. For example, to get all of the names and types of the top level fields on Section, you can run a query like this:

```
curl http://localhost:5000/svc/ptolemy/v1/query --data 'query={__type(name:"Section"){name, fields { name, type { name, kind}}}}'
```

There are some sample queries as well under ./sample-queries. For example

```
curl http://localhost:5000/svc/ptolemy/v1/query -d@'sample-queries/allfields-frags.txt'
```

# SSL

Ptolemy will be exposed to the outside world and will be using SSL/HTTPS to encrypt the query and the authentication (Bearer Token)

## Signed Certs

To create signed certs for prod environments, follow this guide on [How to get a valid SSL Certificate on Shipment](http://blog.harbor.inturner.io/articles/shipment-ssl-howto/)

To make things easier, there is a script in the `certs` folder called `makeKeyCsr.sh` that will create the key and csr files for you without the need for prompting or pasting your domain name everywhere. Pass in the FQDN as the first argument.

```
./makeKeyCsr.sh coredev-capi-ptolemy.prod.services.ec2.dmtio.net
```

## Self-signed Certs

For non-prod environments we can use self signed certs. Here's how to generate one.

### Using the script

In the certs directory, just run the `makeSelfCert.sh` script, passing the FQDN as the only argument. For example:

```
./makeSelfCert.sh coredev-capi-ptolemy.integration.services.ec2.dmtio.net
```

Or you can run it...

### By hand

First, create the signing key (.key) and the certificate request (.csr). **Replace the domain name** with the FQDN for the service you are creating the keys for. Just for form sake, this should match the FQDN used in the prompts.

```
openssl req -newkey rsa:2048 -nodes \
    -keyout coredev-capi-ptolemy.uat.services.ec2.dmtio.net.key \
    -out coredev-capi-ptolemy.uat.services.ec2.dmtio.net.csr
```

Second, create the self signed certificate (.cer) using the .key and .csr files from the previous step.

```
openssl x509 -signkey coredev-capi-ptolemy.uat.services.ec2.dmtio.net.key \
    -in coredev-capi-ptolemy.uat.services.ec2.dmtio.net.csr \
    -req -days 365 \
    -out coredev-capi-ptolemy_uat_services_ec2_dmtio_net_cert.cer
```

In harbor under containers, once you've switched the protocol to https (and likely the public port to 443), then you'll copy and paste the files into these settings:

- .key (private key) -> Private Key
- .cer (certificate) -> Public Key Certificate

Leave the **Certificate Chain** field blank.

# Timing tests

To test the timing of the calls you can use the `curl-format.txt` file with curl. This works for any curl calls, but documenting its use here for convenience. It can be used with the `-w` flag in curl. The example below shows its usage to get the timing of a call through curl to a resource.

- __You will need to replace the bearer token with the correct token in order for the call to work.__
- __Here we are also using the `--compressed` flag, but it is optional (but encouraged).__
- __We are also using `--insecure` since this example is against pre-prod which uses a self-signed cert.__

```
curl -vso /dev/null \
    -w"@curl-format.txt" \
    --compressed --insecure \
    -H'Authorization: bearer 12345' \
    https://coredev-capi-ptolemy.integration.services.ec2.dmtio.net/svc/ptolemy/v1/query \
    -d'@./sample-queries/cerebro-section.txt'
```