/* eslint camelcase:0 */
'use strict';

module.exports = function(grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        clean: {
            folder: 'dist'
        },
        eslint: {
            target: [
                'Gruntfile.js',
                'src/**/*.js',
                'test/**/*.js'
            ]
        },
        babel: {
            options: {
                sourceMap: true
            },
            dist: {
                files: [{
                  expand: true,
                  cwd: 'src',
                  src: ['**/*.js'],
                  dest: 'dist/'
                }]
            }
        },
        mocha_istanbul: {
            options: {
                require: [
                    'babel-polyfill'
                ],
                mask: '**/*.spec.js',
                scriptPath: require.resolve('babel-istanbul/lib/cli'),
                nodeExec: require.resolve('babel-cli/bin/babel-node'),
                mochaOptions: ['--compilers', 'js:babel-core/register']
            },
            coverage: {
                src: 'test',
                options: {
                    reporter: 'spec'
                }
            },
            bambooCoverage: {
                src: 'test',
                options: {
                    reporter: 'mocha-bamboo-reporter'
                }
            }
        },
        istanbul_check_coverage: {
            default: {
                options: {
                    coverageFolder: 'coverage',
                    // temporarily below 90 as the project is just starting.
                    check: {
                        lines: 90,
                        statements: 90,
                        branches: 50,
                        functions: 90
                    }
                }
            }
        }
    });

    grunt.registerTask('jshint', ['eslint']);

    /* register tasks */
    grunt.registerTask('build', [
        'clean',
        'babel'
    ]);

    grunt.registerTask('test', [
        'clean',
        'eslint',
        'mocha_istanbul:coverage',
        'istanbul_check_coverage'
    ]);

    grunt.registerTask('buildTest', [
        'clean',
        'mocha_istanbul:bambooCoverage',
        'istanbul_check_coverage',
        'build'
    ]);
};
