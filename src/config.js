const pkg = require('../package.json');
const urlParse = require('url').parse;

const hostname = process.env.HOSTNAME || require('os').hostname();
const DATA_SERVICE_PATH = process.env.DATA_SERVICE_PATH;
const DATA_SERVICE_HOSTNAME = process.env.DATA_SERVICE_HOSTNAME;
const DATA_SERVICE_PORT = validateNumber(process.env.DATA_SERVICE_PORT);
const DATA_SERVICE_TIMEOUT = validateNumber(process.env.DATA_SERVICE_TIMEOUT);
const BEARER_TOKEN = process.env.BEARER_TOKEN;
const HOST = process.env.HOST;
const PORT = validateNumber(process.env.PORT);
const ENVIRONMENT = process.env.ENVIRONMENT;
const STATSD_URL = parseStatsdUrl(process.env.STATSD_HOST);

const MAX_BATCH_SIZE = (
    isNaN(process.env.MAX_BATCH_SIZE) ?
    Math.Inifinity :
    Math.abs(parseInt(process.env.MAX_BATCH_SIZE, 10))
);

const LOADER_CACHE_ENABLED = (process.env.LOADER_CACHE_ENABLED === '1');
const LOADER_CACHE_TTL = validateNumber(process.env.LOADER_CACHE_TTL);

function parseStatsdUrl(host) {
    let url;
    if ( host ) {
        url = urlParse(host);
    }
    if ( !url || !url.host ) {
        console.error(
            'Unable to get statsd server from STATSD_HOST environment ' +
            'variable, setting to localhost and continuing.'
        );
        url = urlParse('http://localhost:8125');
    }
    return url;
}

/**
* Validate that a number is a number and return the correct number
*
* @param  {String} field the value to test and parse
* @return {Integer}       Integer representation of the number
*/
function validateNumber(field) {
    if ( !field ) {
        throw new Error('Required field was not provided');
    }
    if (isNaN(field) || field < 0) {
        throw new Error(`${field} is not evaluating to a positive number!`);
    }
    return Math.abs(parseInt(field, 10));
}

const configRouteHandler = function healthCheck(req, res) {
    return res({
        status: 200,
        hostname,
        productName: pkg.name,
        version: pkg.version,
        environment: ENVIRONMENT,
        application: {
            DATA_SERVICE_PATH,
            DATA_SERVICE_HOSTNAME,
            DATA_SERVICE_PORT,
            MAX_BATCH_SIZE,
            LOADER_CACHE_ENABLED,
            LOADER_CACHE_TTL,
            STATSD_URL
        }
    });
};

/**
* Create the config object that will be exported
*/
const config = {
    getHttpBasePath: () => DATA_SERVICE_PATH,
    getHttpHostName: () => DATA_SERVICE_HOSTNAME,
    getHttpPort: () => DATA_SERVICE_PORT,
    getHttpTimeout: () => DATA_SERVICE_TIMEOUT,
    getBearerToken: () => BEARER_TOKEN,
    getMaxBatchSize: () => MAX_BATCH_SIZE,
    getLoaderCacheEnabled: () => LOADER_CACHE_ENABLED,
    getLoaderCacheTtl: () => LOADER_CACHE_TTL,
    getHost: () => HOST,
    getPort: () => PORT,
    getStatsdUrl: () => STATSD_URL,
    getEnvironment: () => ENVIRONMENT,
    getHostname: () => hostname,
    getVersion: () => pkg.version,
    getProductName: () => pkg.name,
    configRouteHandler
};

// Loop through all config options and ensure they are set
const errors = [];

for (const f in config) {
    if (config.hasOwnProperty(f) && typeof config[f] === 'function') {
        const v = config[f];
        if ( v === null || v === undefined ) {
            errors.push(new Error(
                `The underlying variable in the config function '${f}' is not
                set correctly`
            ));
        }
    }
}

if(errors.length) {
    throw errors.join('\n');
}

module.exports = config;
