/* eslint max-len:0 */
import {
    GraphQLUnionType
} from 'graphql';

import AnimationValue
    from './RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/AnimationValue';
import EmbedValue
    from './RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/EmbedValue';
import LiveVideoValue
    from './RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/LiveVideoValue';
import Video360Value
    from './RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/Video360Value';
import HyperlinkValue
    from './RelatedMedia/RelatedMediaItem/Types/ValueWrappers/ValueTarget/HyperlinkValue';

import Article from './Article';
import Audio from './Audio';
import Video from './Video';
import Gallery from './Gallery';
import VideoCollection from './VideoCollection';
import Collection from './Collection';
import Image from './Image';
import Section from './Section';
import Show from './Show';
import Special from './Special';
import Profile from './Profile';
import CoverageContainer from './CoverageContainer';
import RefError from './RefError';
import UnknownType from './UnknownType';

const Reference = new GraphQLUnionType({
    name: 'Reference',
    types: [
        AnimationValue,
        EmbedValue,
        LiveVideoValue,
        Video360Value,
        HyperlinkValue,
        Article,
        Audio,
        Video,
        Gallery,
        VideoCollection,
        Collection,
        Image,
        Section,
        Show,
        Special,
        Profile,
        CoverageContainer,
        RefError,
        UnknownType
    ],
    resolveType(obj, _, {rootValue: {log}}) {
        switch(obj.type) {
            case 'animation' : return AnimationValue;
            case 'embed' : return EmbedValue;
            case 'liveVideo' : return LiveVideoValue;
            case 'video360' : return Video360Value;
            case 'hyperlink' : return HyperlinkValue;
            case 'audio': return Audio;
            case 'article': return Article;
            case 'video': return Video;
            case 'gallery': return Gallery;
            case 'videoCollection': return VideoCollection;
            case 'collection': return Collection;
            case 'image': return Image;
            case 'section': return Section;
            case 'show': return Show;
            case 'special': return Special;
            case 'profile': return Profile;
            case 'coverageContainer': return CoverageContainer;
            case 'error': return RefError;
            default:
                log(['warn', 'uknownType'],
                    `Unknown or unsuported 'Reference' id: "${obj.id}", ` +
                    `type "${obj.type}"`
                );
                return UnknownType;
        }
    }
});

export default Reference;
