/* eslint max-len:0 */
import {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLBoolean,
    GraphQLList,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
    GraphQLNonNull
} from 'graphql';

import Composite from './Composite';
import Reference from './Reference';
import Search from './Search';


const Query = new GraphQLObjectType({
    name: 'Query',
    fields: () => ({
        section: {
            type: Composite,
            deprecationReason: 'The backing application is marked to be decommissioned and feature development has been halted.',
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLID)
                },
                start: {
                    type: GraphQLInt
                },
                rows: {
                    type: GraphQLInt
                },
                sendToApps: {
                    type: GraphQLBoolean
                },
                disableInfiniteScroll: {
                    type: GraphQLBoolean
                }
            },
            resolve({db},
                {id, start, rows, sendToApps, disableInfiniteScroll}
            ) {
                let path = `${id}`;
                const qs = [];
                if (start) {
                    path += `/start:${start}`;
                }
                if (rows) {
                    path += `/rows:${rows}`;
                }
                if (sendToApps !== undefined) {
                    qs.push(`sendToApps=${sendToApps}`);
                }
                if (disableInfiniteScroll !== undefined) {
                    qs.push(`disableInfiniteScroll=${disableInfiniteScroll}`);
                }

                if (qs.length) {
                    path += `?${qs.join('&')}`;
                }

                return db.getSection(path);
            }
        },
        doc: {
            type: new GraphQLList(Reference),
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLID)
                }
            },
            resolve({db}, {id}) {
                return db.getDocs(id);
            }
        },
        search: {
            type: Search,
            args: {
                query: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve({db}, {query}) {
                return db.getSearch(query);
            }
        }
    })
});

const Schema = new GraphQLSchema({
    query: Query
});

export default Schema;
