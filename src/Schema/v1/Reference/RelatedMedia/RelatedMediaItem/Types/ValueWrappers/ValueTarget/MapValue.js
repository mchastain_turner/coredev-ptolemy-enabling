import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';
import {
    ObjectType
} from '../../../../../../ObjectType';

const MapValue = new GraphQLObjectType({
    name: 'MapValue',
    fields: () => ({
        type: {
            type: GraphQLString
        },
        caption: {
            type: GraphQLString
        },
        lastModifiedUser: {
            type: GraphQLString
        },
        lastModifiedDate: {
            type: GraphQLString
        },
        dateCreated: {
            type: GraphQLString
        },
        images: {
            type: ObjectType
        },
        mediaType: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        slug: {
            type: GraphQLString
        },
        tags: {
            type: new GraphQLList(GraphQLString)
        },
        data: {
            type: ObjectType
        }
    })
});
export default MapValue;
