import chai from 'chai';
import {stub} from 'sinon';
import {graphql} from 'graphql';
import {v1 as Schema} from '../../../src/Schema';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

/**
 * Homepage was pulled directly from the didgeridoo api and stored as a file,
 * which is used here as a fixture. Here we load it up as the 'db' to be
 * used to get dater from the fixture
 */

import homepage from './homepage.fixture.json';
// import interactiveCard from './interactive-card.fixture.json';
import article from './Reference/article.fixture.json';
import video from './Reference/video.fixture.json';
import gallery from './Reference/gallery.fixture.json';
import videoCollection from './Reference/videoCollection.fixture.json';
import collection from './Reference/collection.fixture.json';
import image from './Reference/image.fixture.json';

const getSectionStub = stub();
const getSearchStub = stub();
const getDocsStub = stub();

function clone(json) {
    return JSON.parse(JSON.stringify(json));
}

getSectionStub.withArgs('cnn/homepage').returns(
    Promise.resolve(clone(homepage))
);

getSectionStub.withArgs('cnn/homepage/start:10/rows:10').returns(
    Promise.resolve(clone(homepage))
);

getSearchStub.withArgs('id:article_test').returns(
    Promise.resolve({
        docs: clone(article)
    })
);

getDocsStub.withArgs('article_test').returns(
    Promise.resolve([article[0]])
);

getSearchStub.withArgs('image_test').returns(
    Promise.resolve({
        docs: clone(image)
    })
);

const logStub = stub();
let loadStub;

beforeEach(function() {
    loadStub = stub();

    loadStub.withArgs('article_test')
        .returns(article[0]);
    loadStub.withArgs('collection_test')
        .returns(collection[0]);
    loadStub.withArgs('gallery_test')
        .returns(gallery[0]);
    loadStub.withArgs('image_test')
        .returns(image[0]);
    loadStub.withArgs('videoCollection_test')
        .returns(videoCollection[0]);
    loadStub.withArgs('video_test')
        .returns(video[0]);
});


const qopts = {
    db: {
        getSection: getSectionStub,
        getSearch: getSearchStub,
        getDocs: getDocsStub,
        docLoader: {
            load: loadStub
        },
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent, fieldName, fallback) {
            if ( parent[fieldName] && parent[fieldName].handleType === 'reference' ) {
                return loadStub(parent[fieldName].handle.referenceId);
            } else if ( parent[fieldName] && parent[fieldName].handleType === 'value' ) {
                return parent[fieldName].handle;
            } else if (fallback) {
                return loadStub(fallback);
            } else {
                return null;
            }
        }
    },
    log: logStub
};

describe('Didgeridoo Schema Query Tests', function() {
    describe('Arguments', function() {

        const getSectionStub = stub().returns(
            Promise.resolve(clone(homepage))
        );

        const qopts = {
            db: {
                getSection: getSectionStub
            },
            log: logStub
        };

        it('should call the stub with no arguments', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage');

            getSectionStub.reset();
        });

        it('should call the stub with start and rows', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:20) {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage/start:10/rows:20');
            getSectionStub.reset();
        });

        it('should call the stub with start, rows, and sendToApps true', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:20 sendToApps:true) {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage/start:10/rows:20?sendToApps=true');
            getSectionStub.reset();
        });

        it('should call the stub with start, rows, and sendToApps false', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:20 sendToApps:false) {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage/start:10/rows:20?sendToApps=false');
            getSectionStub.reset();
        });

        it('should call the stub with start, rows, and disableInfiniteScroll true ', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:20 disableInfiniteScroll:true) {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage/start:10/rows:20?disableInfiniteScroll=true');
            getSectionStub.reset();
        });

        it('should call the stub with start, rows, sendToApps, and disableInfiniteScroll true ', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:20 sendToApps:true disableInfiniteScroll:true) {
                        cards {sendToApps}
                    }
                }
            `;
            await graphql(Schema, query, qopts);
            getSectionStub.should.have.been.calledOnce
                .and.calledWith('cnn/homepage/start:10/rows:20?sendToApps=true&disableInfiniteScroll=true');
            getSectionStub.reset();
        });
    });

    describe('Basic Queries', function() {
        it('should return a basic field from a basic homepage query', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        type
                    }
                }
            `;

            const expected = {
                section: {
                    type: 'section'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a basic field from a basic homepage query with rows and start', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage" start:10 rows:10) {
                        type
                    }
                }
            `;

            const expected = {
                section: {
                    type: 'section'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all top level properties of the section (everything but cuts)', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        zone
                        firstPublishDate
                        id
                        language
                        lastPublishDate
                        links {
                            next
                            self
                            prev
                        }
                        properties
                        section
                        status {
                            state
                        }
                        topics
                        type
                        url
                        branding
                        dataSource
                        sectionDisplayName
                    }
                }
            `;

            const expected = {
                section: {
                    zone: {
                        mobile: {
                            limit: 100,
                            zoneType: 'card',
                            displayLabel: false,
                            label: 'mobile',
                            isOrphanage: false,
                            showAds: true,
                            id: 'mobile',
                            isPublishable: true,
                            isDefault: false,
                            priority: 'relevant',
                            branding: 'default',
                            options: {
                                theme: 'light',
                                layout: 'balanced',
                                transparent: false,
                                extraordinary: false
                            }
                        }
                    },
                    firstPublishDate: '2013-12-13T16:06:53Z',
                    id: 'h_e91557ab467399092f1e008f7077315b',
                    language: 'en',
                    lastPublishDate: '2016-04-28T16:49:54Z',
                    links: {
                      next: '/svc/mcs/v3/composites/sections/cnn/homepage/rows:10/start:10',
                      self: '/svc/mcs/v3/composites/sections/cnn/homepage/rows:10/start:0',
                      prev: null
                    },
                    properties: null,
                    section: 'mobile-app-manual',
                    status: {
                      state: 'published'
                    },
                    topics: [],
                    type: 'section',
                    url: 'http://www.cnn.com/mobile-app-manual/index.html',
                    branding: 'default',
                    dataSource: 'cnn',
                    sectionDisplayName: 'Mobile App Manual'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });

        });
    });

    describe('Cards Queries', function() {
        it('should return a top level properties on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            headline
                            location
                            referenceType
                            type
                            url
                            lastPublishDate
                            sendToApps
                            wordCount
                            storyType
                            referenceUrl
                            referenceId
                        }
                    }
                }
            `;

            const expected = {
                section: {
                    cards: [
                        {
                            headline: 'GOP establishment begins warming to Trump',
                            lastPublishDate: '2016-04-28T14:16:29Z',
                            location: 'mobile',
                            referenceUrl: '/svc/mcs/v3/docs/h_971ac7af629c706b72381a603750295b',
                            sendToApps: true,
                            storyType: 'Story',
                            type: 'reference',
                            url: 'http://www.cnn.com/2016/04/28/politics/gop-establishment-donald-trump/index.html',
                            wordCount: 1185,
                            referenceType: 'article',
                            referenceId: 'article_test'
                        },
                        {
                            headline: 'Boehner: Cruz is \'Lucifer in the flesh\'',
                            lastPublishDate: '2016-04-28T16:57:45Z',
                            location: 'mobile',
                            referenceUrl: '/svc/mcs/v3/docs/video_test',
                            sendToApps: true,
                            storyType: 'Story',
                            type: 'reference',
                            url: 'http://www.cnn.com/2016/04/28/politics/john-boehner-ted-cruz-lucifer-stanford/index.html',
                            wordCount: 607,
                            referenceType: 'video',
                            referenceId: 'video_test'
                        },
                        {
                            headline: '2016 Met Gala: Red carpet',
                            lastPublishDate: '2016-05-03T02:08:10Z',
                            location: 'mobile',
                            referenceId: 'gallery_test',
                            referenceType: 'gallery',
                            referenceUrl: '/svc/mcs/v3/docs/h_27f84791c5eae684d504c3b4ce5a27b1',
                            sendToApps: true,
                            storyType: null,
                            type: 'reference',
                            url: 'http://www.cnn.com/2016/05/02/entertainment/gallery/2016-met-gala-red-carpet/index.html',
                            wordCount: null
                        },
                        {
                            headline: 'Test video collection card',
                            lastPublishDate: null,
                            location: null,
                            referenceId: 'videoCollection_test',
                            referenceType: 'videoCollection',
                            referenceUrl: null,
                            sendToApps: true,
                            storyType: null,
                            type: 'reference',
                            url: null,
                            wordCount: null
                        },
                        {
                            headline: 'Test collection card',
                            lastPublishDate: null,
                            location: null,
                            referenceId: 'collection_test',
                            referenceType: 'collection',
                            referenceUrl: null,
                            sendToApps: true,
                            storyType: null,
                            type: 'reference',
                            url: null,
                            wordCount: null
                        },
                        {
                            headline: 'HP Mobile Web C1 Web Tag',
                            lastPublishDate: null,
                            location: null,
                            referenceId: null,
                            referenceType: 'interactive',
                            referenceUrl: null,
                            sendToApps: true,
                            storyType: null,
                            type: 'reference',
                            url: null,
                            wordCount: null
                        },
                        {
                            headline: 'Schattenberg test',
                            lastPublishDate: null,
                            location: 'politics-zone-1',
                            referenceId: null,
                            referenceType: 'embed',
                            referenceUrl: null,
                            sendToApps: true,
                            storyType: null,
                            type: 'value',
                            url: null,
                            wordCount: null
                        },
                        {
                            headline: 'Test card - live stream',
                            lastPublishDate: null,
                            location: 'business-zone-2',
                            referenceId: null,
                            referenceType: 'liveVideo',
                            referenceUrl: null,
                            sendToApps: true,
                            storyType: null,
                            type: 'value',
                            url: null,
                            wordCount: null
                       }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return description and banner properties on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            description {
                                id
                                plaintext
                                richtext
                                elements {
                                    ...on Handle {
                                        type
                                        attributes
                                        target {
                                            type
                                            subtype
                                            referenceUrl
                                            referenceUri
                                        }
                                    }
                                    ...on Embed {
                                        type
                                        attributes
                                    }
                                }
                            }
                            banner {
                                id
                                plaintext
                                richtext
                                elements {
                                    ...on Handle {
                                        type
                                        attributes
                                        target {
                                            type
                                            subtype
                                            referenceUrl
                                            referenceUri
                                        }
                                    }
                                    ...on Embed {
                                        type
                                        attributes
                                    }
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                section: {
                    cards: [
                        {
                            description: [
                                {
                                    id: 'paragraph_DA841A72-1E22-3F33-C44A-5D77F7B29E6A',
                                    plaintext: 'In February, Senate Majority Whip John Cornyn bluntly said that a Donald Trump nomination could be an "albatross" to his Republican Party.',
                                    richtext: 'In February, Senate Majority Whip John Cornyn bluntly said that a Donald Trump nomination could be an "albatross" to his Republican Party.',
                                    elements: []
                                 }
                            ],
                            banner: [
                                {
                                    id: '0',
                                    plaintext: 'Singing a different tune',
                                    richtext: 'Singing a different tune',
                                    elements: []
                                }
                            ]
                        },
                        {
                            description: [
                                {
                                    id: 'paragraph_0B368973-2F82-AD95-8B29-5D75C63A15E7',
                                    plaintext: 'Former House Speaker John Boehner called Republican presidential candidate Ted Cruz "Lucifer in the flesh," in a withering interview at Stanford University published Thursday. ',
                                    richtext: 'Former House Speaker John Boehner called Republican presidential candidate Ted Cruz "Lucifer in the flesh," in a withering interview at Stanford University published Thursday. ',
                                    elements: []
                                }
                            ],
                            banner: [
                                {
                                    id: '0',
                                    plaintext: null,
                                    richtext: null,
                                    elements: []
                                }
                            ]
                        },
                        {
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_D07BAD62-BA72-DD2A-A48F-742F7134C14E',
                                    plaintext: 'Poppy Delevinge attends the "Manus x Machina: Fashion In An Age of Technology" Costume Institute Gala at the Metropolitan Museum of Art.',
                                    richtext: 'Poppy Delevinge attends the "Manus x Machina: Fashion In An Age of Technology" Costume Institute Gala at the Metropolitan Museum of Art.'
                                }
                            ],
                            banner: [
                                {
                                    elements: [],
                                    id: '0',
                                    plaintext: null,
                                    richtext: null
                                }
                            ]
                        },
                        {
                            banner: [
                                {
                                    elements: [],
                                    id: '0',
                                    plaintext: null,
                                    richtext: null
                                }
                            ],
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_AE3E5B42-3128-6D74-F464-BFAE86D9B09E',
                                    plaintext: null,
                                    richtext: null
                                }
                            ]
                        },
                        {
                            banner: [
                                {
                                    elements: [],
                                    id: '0',
                                    plaintext: null,
                                    richtext: null
                                }
                            ],
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_AE3E5B42-3128-6D74-F464-BFAE86D9B09E',
                                    plaintext: null,
                                    richtext: null
                                }
                            ]
                        },
                        {
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_AE3E5B42-3128-6D74-F464-BFAE86D9B09E',
                                    plaintext: null,
                                    richtext: null
                                }
                            ],
                            banner: [
                                {
                                    elements: [],
                                    id: '0',
                                    plaintext: null,
                                    richtext: null
                                }
                            ]
                        },
                        {
                            banner: null,
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_C044FC6E-8F7A-8BFA-75A4-12A6D1BFC3B7',
                                    plaintext: 'duplicate',
                                    richtext: 'duplicate'
                                }
                            ]
                        },
                        {
                            banner: [
                                {
                                    elements: [],
                                    id: '0',
                                    plaintext: 'check banner',
                                    richtext: 'check banner'
                                }
                            ],
                            description: [
                                {
                                    elements: [],
                                    id: 'paragraph_E860A810-ABD9-7714-B74D-B0AE845E0FB9',
                                    plaintext: 'test live stream',
                                    richtext: 'test live stream'
                                }
                      ]
                    }
            ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return a dump of the cuts properties on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            cuts
                        }
                    }
                }
            `;

            const result = await graphql(Schema, query, qopts);
            result.should.have.property('data')
                .and.have.property('section')
                .and.have.property('cards')
                .and.have.property('length', 8);

            const articleResult = result.data.section.cards[0];
            const videoResult = result.data.section.cards[1];
            const galleryResult = result.data.section.cards[2];
            const interactiveResult = result.data.section.cards[3];

            articleResult.cuts.should.to.deep.equal(homepage.cards[0].cuts);
            videoResult.cuts.should.to.deep.equal(homepage.cards[1].cuts);
            galleryResult.cuts.should.to.deep.equal(homepage.cards[2].cuts);
            interactiveResult.cuts.should.to.deep.equal(homepage.cards[3].cuts);
        });

        it('should return a dump of the cuts properties for the selected cuts on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            cuts(sizes:["small4to3", "large4to3"])
                        }
                    }
                }
            `;

            const expecteds = [
                {
                    large4to3: {
                        height: 480,
                        width: 640,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160427133851-01-trump-foreign-policy-0427-video-synd-2.jpg'
                    },
                    small4to3: {
                        height: 186,
                        width: 248,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160427133851-01-trump-foreign-policy-0427-assign.jpg'
                    }
                },
                {
                    small4to3: {
                        height: 186,
                        width: 248,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160421060543-john-boehner-october-29-2015-assign.jpg'
                    },
                    large4to3: {
                        height: 480,
                        width: 640,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160421060543-john-boehner-october-29-2015-video-synd-2.jpg'
                    }
                },
                {
                    small4to3: {
                        height: 186,
                        width: 248,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160502190404-01-met-gala-2016-assign.jpg'
                    },
                    large4to3: {
                        height: 480,
                        width: 640,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/160502190404-01-met-gala-2016-video-synd-2.jpg'
                    }
                },
                {
                    small4to3: {
                        height: 186,
                        width: 248,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/150325082152-social-gfx-cnn-logo-assign.jpg'
                    },
                    large4to3: {
                        height: 480,
                        width: 640,
                        url: 'http://i2.cdn.turner.com/cnnnext/dam/assets/150325082152-social-gfx-cnn-logo-video-synd-2.jpg'
                    }
                }
            ];

            const result = await graphql(Schema, query, qopts);
            result.should.have.property('data')
                .and.have.property('section')
                .and.have.property('cards')
                .and.have.property('length', 8);

            const articleResult = result.data.section.cards[0];
            const videoResult = result.data.section.cards[1];
            const galleryResult = result.data.section.cards[2];
            const interactiveResult = result.data.section.cards[3];

            articleResult.cuts.should.to.deep.equal(expecteds[0]);
            videoResult.cuts.should.to.deep.equal(expecteds[1]);
            galleryResult.cuts.should.to.deep.equal(expecteds[2]);
            interactiveResult.cuts.should.to.deep.equal(expecteds[3]);
        });

    });

    describe('Reference Fragment Queries', function() {
        it('should return the reference from each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            reference {
                                ...on EmbedValue {
                                    type
                                    slug
                                }
                                ... on LiveVideoValue {
                                    type
                                    liveStream
                                    playVidLiveStream
                                }
                                ...on Article {
                                    id
                                    type
                                }
                                ...on Video {
                                    id
                                    type
                                }
                                ...on Gallery {
                                    id
                                    type
                                }
                                ...on VideoCollection {
                                    id
                                    type
                                }
                                ...on Collection {
                                    id
                                    type
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                section: {
                    cards: [
                        {
                            reference: {
                                id: 'article_test',
                                type: 'article'
                            }
                        },
                        {
                            reference: {
                                id: 'video_test',
                                type: 'video'
                            }
                        },
                        {

                            reference: {
                                id: 'gallery_test',
                                type: 'gallery'
                            }
                        },
                        {

                            reference: {
                                id: 'videoCollection_test',
                                type: 'videoCollection'
                            }
                        },
                        {

                            reference: {
                                id: 'collection_test',
                                type: 'collection'
                            }
                        },
                        {
                            reference: null
                        },
                        {
                            reference: {
                                type: 'embed',
                                slug: 'Schattenberg test'
                            }
                        },
                        {
                            reference: {
                                type: 'liveVideo',
                                liveStream: 'live2',
                                playVidLiveStream: 'live2'
                            }
                        }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('Target Fragment Queries', function() {
        it('should return resolved target for each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            target {
                                ...on EmbedValue {
                                    type
                                    slug
                                }
                                ... on LiveVideoValue {
                                    type
                                    liveStream
                                    playVidLiveStream
                                }
                                ...on Article {
                                    id
                                    type
                                }
                                ...on Video {
                                    id
                                    type
                                }
                                ...on Gallery {
                                    id
                                    type
                                }
                                ...on VideoCollection {
                                    id
                                    type
                                }
                                ...on Collection {
                                    id
                                    type
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                section: {
                    cards: [
                        {
                            target: {
                                id: 'article_test',
                                type: 'article'
                            }
                        },
                        {
                            target: {
                                id: 'video_test',
                                type: 'video'
                            }
                        },
                        {

                            target: {
                                id: 'gallery_test',
                                type: 'gallery'
                            }
                        },
                        {

                            target: {
                                id: 'videoCollection_test',
                                type: 'videoCollection'
                            }
                        },
                        {

                            target: {
                                id: 'collection_test',
                                type: 'collection'
                            }
                        },
                        {
                            target: null
                        },
                        {
                            target: {
                                type: 'embed',
                                slug: 'Schattenberg test'
                            }
                        },
                        {
                            target: {
                                type: 'liveVideo',
                                liveStream: 'live2',
                                playVidLiveStream: 'live2'
                            }
                        }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('Resource Fragment Queries', function() {
        it('should return description and banner properties on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            resource {
                                ...on AnimationValue {
                                    type
                                    slug
                                }
                                ...on Image {
                                    id
                                    type
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                section: {
                    cards: [
                        {
                            resource: {
                                type: 'animation',
                                slug: 'Color Bars'
                            }
                        },
                        {
                            resource: {
                                id: 'image_test',
                                type: 'image'
                            }
                        },
                        {
                            resource: null
                        },
                        {
                            resource: null
                        },
                        {
                            resource: null
                        },
                        {
                            resource: null
                        },
                        {
                            resource: null
                        },
                        {
                            resource: {
                                type: 'animation',
                                slug: 'Cliff Animation Test'
                            }
                        }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('Reference doc by id Query', function() {
        it('should return details of an article for the doc query', async function() {
            const query = `
                query Reference {
                    doc(id:"article_test") {
                        ...on Article {
                            id
                            type
                        }
                        ...on Video {
                            id
                            type
                        }
                        ...on Gallery {
                            id
                            type
                        }
                        ...on VideoCollection {
                            id
                            type
                        }
                        ...on Collection {
                            id
                            type
                        }
                    }
                }
            `;

            const expected = {
                doc: [
                    {
                        id: 'article_test',
                        type: 'article'
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('Reference search by id Query', function() {
        it('should return details of an article for the doc query', async function() {
            const query = `
                query Reference {
                    search(query:"id:article_test") {
                        docs {
                            ...on Article {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                search: {
                    docs: [
                        {
                            id: 'article_test',
                            type: 'article'
                        }
                    ]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });
    });

    describe('extensionAttributes', function() {
        it('should return a dump of the cuts properties for the selected cuts on each card', async function() {
            const query = `
                query Section {
                    section(id:"cnn/homepage") {
                        cards {
                            extensionAttributes{
                                branding
                                captions
                                dateCreated
                                flag
                                flagColor
                                kicker{
                                    id
                                    plaintext
                                    richtext
                                    elements {
                                        ...on Handle {
                                            type
                                            attributes
                                            target {
                                                type
                                                subtype
                                                referenceUrl
                                                referenceUri
                                            }
                                        }
                                        ...on Embed {
                                            type
                                            attributes
                                        }
                                    }
                                    format
                                }
                            }
                        }
                    }
                }
            `;

            const expected = {
                branding: 'default',
                captions: {
                    'image_B6567250-63B3-B34C-B61C-50E1B8B35877': ''
                },
                dateCreated: '2016-10-11T13:09:50Z',
                flag: '',
                flagColor: '',
                kicker: [
                    {
                        id: '0',
                        plaintext: null,
                        richtext: null,
                        elements: [],
                        format: []
                    }
                ]
            };

            const result = await graphql(Schema, query, qopts);
            result.should.have.property('data')
                .and.have.property('section')
                .and.have.property('cards')
                .and.have.property('length', 8);

            const embedCard = result.data.section.cards[6];
            chai.expect(embedCard.extensionAttributes).to.deep.equal(expected);
        });
    });

});
