import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './coverageContainer.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/CoverageContainer';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub
    }
};

describe('Didgeridoo CoverageContainer Schema Tests', function() {
    const Query = new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            coverageContainer: {
                type: TypeSchema,
                resolve() {
                    return fixture[0];
                }
            }
        })
    });

    const Schema = new GraphQLSchema({
        query: Query
    });

    it('should return a field on an coverageContainer', async function() {
        const query = `
            query CoverageContainer {
                coverageContainer {
                    id
                    type
                }
            }
        `;

        const expected = {
            coverageContainer: {
                id: 'coverageContainer_test',
                type: 'coverageContainer'
            }
        };

        const result = await graphql(Schema, query, qopts);
        result.should.to.deep.equal( { data: expected });
    });

    it('should return all fields on an coverageContainer', async function() {
        const query = `
            query CoverageContainer {
                coverageContainer {
                    id
                    sourceId
                    type
                    dataSource
                    schema
                    schemaVersion
                    status {state}
                    firstPublishDate
                    lastPublishDate
                    lastModifiedDate
                    slug
                    url
                    title
                    label
                    targetUrl
                    attributes
                    topics {
                        label
                        class
                        id
                        topicID
                        confidenceScore
                        leafNode
                        parentPathIds
                    }
                    relatedMedia {
                        hasImage
                        hasGallery
                        hasVideo
                        hasVideoCollection
                        hasInteractive
                        media {
                            ...on VideoReference {
                                id
                                referenceId
                                type
                                location
                                headline
                                description {
                                    id
                                    plaintext
                                    richtext
                                    elements {
                                        ...on Handle {
                                            type
                                            attributes
                                            target {
                                                type
                                                subtype
                                                referenceUrl
                                                referenceUri
                                            }
                                        }
                                        ...on Embed {
                                            type
                                            attributes
                                        }
                                    }
                                }
                                url
                                cvpXmlUrl
                                autoplay
                                duration
                                referenceType
                                referenceUrl
                                referenceUri
                                cuts
                                auxiliaryText
                            }
                            ...on VideoCollectionReference {
                                id
                                referenceId
                                type
                                headline
                                description {
                                    id
                                    plaintext
                                    richtext
                                    elements {
                                        ...on Handle {
                                            type
                                            attributes
                                            target {
                                                type
                                                subtype
                                                referenceUrl
                                                referenceUri
                                            }
                                        }
                                        ...on Embed {
                                            type
                                            attributes
                                        }
                                    }
                                }
                                referenceType
                                referenceUrl
                                referenceUri
                                location
                                url
                                cuts
                            }
                            ...on ImageValue {
                                id
                                imageId
                                type
                                referenceUrl
                                referenceUri
                                location
                                slug
                                dam_id
                                photographer
                                caption
                                cuts
                            },
                            ...on ArticleReference {
                                id
                                referenceId
                                type
                                location
                                headline
                                description {
                                    id
                                    plaintext
                                    richtext
                                    elements {
                                        ...on Handle {
                                            type
                                            attributes
                                            target {
                                                type
                                                subtype
                                                referenceUrl
                                                referenceUri
                                            }
                                        }
                                        ...on Embed {
                                            type
                                            attributes
                                        }
                                    }
                                    format
                                }
                                banner {
                                    id
                                    plaintext
                                    richtext
                                    elements {
                                        ...on Handle {
                                            type
                                            attributes
                                            target {
                                                type
                                                subtype
                                                referenceUrl
                                                referenceUri
                                            }
                                        }
                                        ...on Embed {
                                            type
                                            attributes
                                        }
                                    }
                                    format
                                }
                                url
                                referenceType
                                referenceUrl
                                referenceUri
                                sendToApps
                                cuts
                                auxiliaryText
                            }
                        }
                        autoZone {
                            auto
                        }
                    }
                    language
                }
            }
        `;
        const expected = {
            data: {
                coverageContainer: fixture[0]
            }
        };

        const result = await graphql(Schema, query, qopts);
        result.should.to.deep.equal(expected);
    });

});
