import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './video.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Video';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub
    }
};

describe('Didgeridoo Video Schema Tests', function() {
    describe('Basic Video schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                video: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an video', async function() {
            const query = `
                query Video {
                    video {
                        id
                        type
                    }
                }
            `;

            const expected = {
                video: {
                    id: 'video_test',
                    type: 'video'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an video', async function() {
            const query = `
                query Video {
                    video {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        description {
                            id
                            plaintext
                            richtext
                            elements {
                                ...on Handle {
                                    type
                                    attributes
                                    target {
                                        type
                                        subtype
                                        referenceUrl
                                        referenceUri
                                    }
                                }
                                ...on Embed {
                                    type
                                    attributes
                                }
                            }
                        }
                        attributes
                        title
                        videoId
                        cvpXmlUrl
                        affiliate
                        showName
                        source
                        franchise
                        branding
                        section
                        categories
                        trt
                        duration
                        cdnUrls
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideoCollection
                            hasInteractive
                            media {
                                ...on ProfileReference {
                                    sendToApps
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    banner {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    banner {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    sendToApps
                                    type
                                    headline
                                    description {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    banner {
                                        id
                                        plaintext
                                        richtext
                                        elements {
                                            ...on Handle {
                                                type
                                                attributes
                                                target {
                                                    type
                                                    subtype
                                                    referenceUrl
                                                    referenceUri
                                                }
                                            }
                                            ...on Embed {
                                                type
                                                attributes
                                            }
                                        }
                                    }
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                    auxiliaryText
                                }
                                ...on ImageValue {
                                    id
                                    imageId
                                    type
                                    referenceUrl
                                    referenceUri
                                    location
                                    slug
                                    dam_id
                                    photographer
                                    caption
                                    cuts
                                }
                            }
                            autoZone {
                                auto
                            }
                        }
                        parent { id }
                        language
                    }
                }
            `;
            const expected = {
                data: {
                    video: fixture[0]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

        it('should return selected cdnUrls when passed arguments', async function() {
            const query = `
                query Video {
                    video {
                        cdnUrls(sizes:["1280x720_3500k", "1920x1080_5500k_mp4", "384x216"])
                    }
                }
            `;
            const expected = {
                data: {
                    video: {
                        cdnUrls: {
                            '1280x720_3500k': 'http://ht.cdn.turner.com/cnn/big/politics/2016/05/02/donald-trump-hillary-clinton-reservation-sot-newday.cnn_1280x720_3500k.scc',
                            '1920x1080_5500k_mp4': 'http://ht.cdn.turner.com/cnn/big/politics/2016/05/02/donald-trump-hillary-clinton-reservation-sot-newday.cnn_ios_5500.mp4',
                            '384x216': 'http://ht.cdn.turner.com/cnn/big/politics/2016/05/02/donald-trump-hillary-clinton-reservation-sot-newday.cnn_384x216_dl.flv'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
