import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import {ObjectType} from '../ObjectType';

const Embed = new GraphQLObjectType({
    name: 'Embed',
    fields: () => ({
        type: { type: GraphQLString },
        attributes: { type: ObjectType }
    })
});

export default Embed;
