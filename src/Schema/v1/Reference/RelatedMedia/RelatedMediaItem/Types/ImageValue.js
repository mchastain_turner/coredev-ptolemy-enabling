/* eslint camelcase:0 */
import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import {ObjectType} from '../../../../ObjectType';
import Reference from '../../../../Reference';

import {
    resolveCuts
} from '../../../resolves';

const ImageValue = new GraphQLObjectType({
    name: 'ImageValue',
    fields: () => ({
        id: { type: GraphQLString },
        imageId: { type: GraphQLString },
        type: { type: GraphQLString },
        referenceUrl: { type: GraphQLString },
        referenceUri: { type: GraphQLString },
        location: { type: GraphQLString },
        slug: { type: GraphQLString },
        dam_id: { type: GraphQLString },
        photographer: { type: GraphQLString },
        caption: { type: GraphQLString },
        sendToApps: { type: GraphQLBoolean },
        cuts: {
            type: ObjectType,
            args: {
                sizes: {
                    type: new GraphQLList(GraphQLString)
                }
            },
            resolve: resolveCuts
        },
        resource: {
            type: Reference,
            resolve(parent,
                args,
                context,
                {rootValue: {db}}
            ) {
                return db.resolveHandle(parent, 'resource');
            }
        }
    })
});

export default ImageValue;
