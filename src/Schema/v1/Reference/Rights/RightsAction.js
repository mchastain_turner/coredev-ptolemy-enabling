import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

const RightsAction = new GraphQLObjectType({
    name: 'RightsAction',
    fields: () => ({
        action: { type: GraphQLString },
        assignee: { type: GraphQLString },
        target: { type: GraphQLString }
    })
});

export default RightsAction;
