import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/ProfileReference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import profile from '../profile.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('profile_test')
    .returns(profile[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent) {
            return loadStub(parent.resource.handle.referenceId);
        }
    },
    log: logStub
};

describe('RelatedMediaItem ProfileReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                profileReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'profile_test',
                            referenceId: 'profile_test',
                            type: 'reference',
                            referenceType: 'profile',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the profile as a hydrated reference', async function() {
            const query = `
                query ProfileReference {
                    profileReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    profileReference: {
                        referenceId: 'profile_test',
                        type: 'reference',
                        referenceType: 'profile',
                        reference: {
                            id: 'profile_test',
                            type: 'profile'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
