/* eslint no-underscore-dangle:0 */
import oboe from 'oboe';
import zlib from 'zlib';
import Boom from 'boom';
import DataLoader from 'dataloader';
import {get, Agent} from 'http';

// HTTTP.get default options
let port = 80;
let hostname = 'localhost';
let basePath = '/';
let timeout = 120000;
let _server;

const keepAliveAgent = new Agent({
    keepAlive: false,
    pool: false
});

const SECTION_PATH = 'composites/sections';
const DOCS_PATH = 'docs';
const SEARCH_PATH = 'search';

let docLoader;

function resolveRef(parent) {
    return docLoader.load(parent.id);
}

function resolveHandle(parent, fieldName, fallbackId) {
    const handle = parent[fieldName];
    if (handle &&
        handle.handleType === 'reference' &&
        handle.handle &&
        handle.handle.referenceId
    ) {
        return docLoader.load(handle.handle.referenceId);
    } else if ( handle &&
        handle.handleType === 'value'
    ) {
        return handle.handle;
    } else if ( fallbackId ) {
        return docLoader.load(fallbackId);
    } else {
        return null;
    }
}


/**
 * Gets a document from the datasource using the search route
 * @param  {String} search The search query to use against the datasource
 * @return {Promise}       A promise that will resolve with the results
 */
function getSearch(search) {
    return makeCall(`${basePath}/${SEARCH_PATH}/${search}`);
}

/**
 * Gets a document from the datasource using the section (composite) route
 * @param  {String} search The composite id to use against the datasource
 * @return {Promise}       A promise that will resolve with the results
 */
function getSection(section) {
    return makeCall(`${basePath}/${SECTION_PATH}/${section}`);
}

/**
 * Gets a document from the datasource using the docs route
 * @param  {String} search The doc[s] id[s] to use against the datasource
 * @return {Promise}       A promise that will resolve with the results
 */
function getDocs(ids) {
    if ( Array.isArray(ids) ) {
        ids = ids
            .join(';');
    }
    return makeCall(`${basePath}/${DOCS_PATH}/${ids}`);
}



/**
 * Wraps the call to the underlying datasource, mostly to add metrics and error
 * handling around the call
 * @param  {String} uri URI for the call to the underlying datasource
 * @return {Promise}    A promise that will resolve with the document
 */
function makeCall(uri) {
    function promiseDocument(resolve, reject) {

        const start = new Date();
        return callGet(uri)
            .then(processDocumentBody)
            .catch(handleGetDocumentError);

        function processDocumentBody(body) {
            const time = new Date() - start;
            _server.log(
                ['info', 'HttpPlugin'],
                `Retrieved document. time: ${time}, uri: ${uri}`
            );
            _server.plugins.statsdClient.timing(
                'getDoc.response.time',
                time
            );

            _server.plugins.statsdClient.increment('getDoc.success');
            _server.plugins.statsdClient.increment('getDoc.failed', 0);
            resolve(body);
        }

        function handleGetDocumentError(err) {
            const time = new Date() - start;
            _server.log(
                ['error', 'HttpPlugin'],
                `Unable to get the document. time: ${time}, uri: ${uri}`
            );
            _server.plugins.statsdClient.timing(
                'getDoc.failed.response.time',
                time
            );
            _server.plugins.statsdClient.increment('getDoc.failed');
            _server.plugins.statsdClient.increment('getDoc.success', 0);
            reject(err);
        }
    }

    return new Promise(promiseDocument);
}

/**
 * Calls "get" on the REST api
 * @param  {String} id Id (composite path) of request to the rest api
 * @return {Promise}      Promise, to be completed later
 */
function callGet(path) {
    function promiseGetId(resolve, reject) {

        const req = get({
            hostname,
            port,
            path,
            headers: {
                accept: 'application/json',
                'accept-encoding': 'gzip,deflate'
            },
            agent: keepAliveAgent,
            timeout
        }, handleGetId );

        req.on('error', function(err) {
            reject(err);
        });

        req.on('timeout', function(err) {
            if (!err) {
                err = new Error('The request timed out');
                err.name = 'REQTIMEOUT';
            }

            _server.log(
                ['error', 'HttpPlugin'],
                'Request timed out for ' +
                `uri: ${path}`
            );

            req.abort();
            reject(err);
        });

        function handleGetId(response) {
            let output;
            if( response.headers['content-encoding'] === 'gzip' ) {
                const gzip = zlib.createGunzip();
                response.pipe(gzip);
                output = gzip;
            } else {
                output = response;
            }

            oboe(output)
            .done(function(body) {
                if ( succeeded(response.statusCode) ) {
                    resolve(body);
                } else {
                    _server.log(
                        ['error', 'HttpPlugin'],
                        'Bad status code. ' +
                        `uri: ${path}, ` +
                        `code: ${response.statusCode}`
                    );
                    reject(Boom.create(
                        response.statusCode,
                        (body.reason ? body.reason : 'General error'),
                        { response: body }
                    ));
                }
            })
            .fail(function(err) {
                reject(Boom.create(
                    503,
                    `Failed to parse body response. ${err.thrown.message}`
                ));
            });
        }

        function succeeded(statusCode) {
            return statusCode >= 200 && statusCode < 300;
        }

    }

    return new Promise(promiseGetId);
}

const db = {
    docLoader,
    getSearch,
    getSection,
    getDocs,
    resolveRef,
    resolveHandle
};

/**
 * "opens" the connection. Really just sets up the "get" function
 * @param  {Object}   req   Request object to attach the db plugin to
 * @param  {Function} reply Callback on success
 */
function openConnection(req, reply) {
    try {
        req.db = db;
        reply.continue();
    } catch (error) {
        reply(error);
    }
}

/**
 * Register this plugin with the caller
 * @param  {Object}   server  Server object to register with
 * @param  {Object}   options Options to use to init this plugin
 * @param  {Function} next    Callback
 */
function register(server, options, next) {
    hostname = options.hostname;
    port = options.port;
    basePath = options.basePath;
    const cacheEnabled = (options.cacheEnabled && options.cacheTtl >= 0);

    docLoader = new DataLoader(
        getDocs,
        {
            cache: cacheEnabled,
            maxBatchSize: options.maxBatchSize
        }
    );

    timeout = options.timeout;

    if (cacheEnabled) {
        setInterval(() => docLoader.clearAll(),
        options.cacheTtl);
    }

    _server = server;
    _server.log(['info', 'HttpPlugin', 'register'],
        'HttpPlugin is registered: ' +
        `host: ${hostname}:${port}/${basePath}, ` +
        `maxBatchSize: ${options.maxBatchSize}`
    );
    server.ext('onRequest', openConnection);
    next();
}

register.attributes = {
    name: 'HttpPlugin'
};

const HttpPlugin = {register, db};
export default HttpPlugin;
