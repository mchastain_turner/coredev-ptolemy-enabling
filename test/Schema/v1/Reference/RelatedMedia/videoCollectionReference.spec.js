import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/VideoCollectionReference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import videoCollection from '../videoCollection.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('videoCollection_test')
    .returns(videoCollection[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent) {
            return loadStub(parent.resource.handle.referenceId);
        }
    },
    log: logStub
};

describe('RelatedMediaItem VideoCollectionReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                videoCollectionReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'videoCollection_test',
                            referenceId: 'videoCollection_test',
                            type: 'reference',
                            referenceType: 'videoCollection',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the videoCollection as a hydrated reference', async function() {
            const query = `
                query VideoCollectionReference {
                    videoCollectionReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    videoCollectionReference: {
                        referenceId: 'videoCollection_test',
                        type: 'reference',
                        referenceType: 'videoCollection',
                        reference: {
                            id: 'videoCollection_test',
                            type: 'videoCollection'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
