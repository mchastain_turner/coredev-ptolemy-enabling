import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/ArticleReference';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

import article from '../article.fixture.json';
import image from '../image.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('article_test')
    .returns(article[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent) {
            return loadStub(parent.resource.handle.referenceId);
        }
    },
    log: logStub
};

describe('RelatedMediaItem ArticleReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                articleReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'article_test',
                            referenceId: 'article_test',
                            type: 'reference',
                            referenceType: 'article',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the article as a hydrated reference', async function() {
            const query = `
                query ArticleReference {
                    articleReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image{
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    articleReference: {
                        referenceId: 'article_test',
                        type: 'reference',
                        referenceType: 'article',
                        reference: {
                            id: 'article_test',
                            type: 'article'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
