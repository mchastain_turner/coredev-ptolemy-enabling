import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import {ObjectType} from '../ObjectType';

const Handle = new GraphQLObjectType({
    name: 'Handle',
    fields: () => ({
        type: { type: GraphQLString },
        attributes: {
            type: ObjectType
        },
        target: {
            type: new GraphQLObjectType({
                name: 'HandleTarget',
                fields: () => ({
                    type: { type: GraphQLString },
                    subtype: { type: GraphQLString },
                    referenceUrl: { type: GraphQLString },
                    referenceUri: { type: GraphQLString }
                })
            })
        }
    })
});

export default Handle;
