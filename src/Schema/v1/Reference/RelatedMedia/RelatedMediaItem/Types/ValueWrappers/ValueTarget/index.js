import {
    GraphQLUnionType
} from 'graphql';
import AnimationValue from './AnimationValue';
import EmbedValue from './EmbedValue';
import LiveVideoValue from './LiveVideoValue';
import MapValue from './MapValue';
import Video360Value from './Video360Value';
import UnknownType from '../../../../../UnknownType';

const ValueTarget = new GraphQLUnionType({
    name: 'ValueTarget',
    types: [
        AnimationValue,
        EmbedValue,
        LiveVideoValue,
        MapValue,
        Video360Value,
        UnknownType
    ],
    resolveType(obj, _, {rootValue: {log}}) {
        switch (obj.type) {
            case 'animation':
                return AnimationValue;
                break;
            case 'embed':
                return EmbedValue;
                break;
            case 'liveVideo':
                return LiveVideoValue;
                break;
            case 'map':
                return MapValue;
                break;
            case 'video360':
                return Video360Value;
                break;
            default:
                log(['warn', 'uknownType'],
                    'Unknown or unsuported \'ValueTarget\' ' +
                    `id: "${obj.id}", ` +
                    `type: "${obj.type}"`
                );
                return UnknownType;
        }
    }
});

export default ValueTarget;
