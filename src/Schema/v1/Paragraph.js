import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString
} from 'graphql';

import Elements from './Elements';
import { ObjectType } from './ObjectType';

const Paragraph = new GraphQLObjectType({
    name: 'Paragraph',
    fields: () => ({
        id: { type: GraphQLString },
        plaintext: { type: GraphQLString },
        richtext: { type: GraphQLString },
        elements: {
            type: new GraphQLList(Elements)
        },
        format: {
            type: new GraphQLList(ObjectType)
        }
    })
});

export default Paragraph;
