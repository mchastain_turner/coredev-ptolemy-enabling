import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Status from '../Status';
import Topic from './Subs/Topic';
import RelatedMedia from './RelatedMedia';
import { ObjectType } from '../ObjectType';

const CoverageContainer = new GraphQLObjectType({
    name: 'CoverageContainer',
    fields: () => ({
        id: { type: GraphQLString },
        sourceId: { type: GraphQLString },
        type: { type: GraphQLString },
        dataSource: { type: GraphQLString },
        schema: { type: GraphQLString },
        schemaVersion: { type: GraphQLInt },
        status: {
            type: Status
        },
        firstPublishDate: { type: GraphQLString },
        lastPublishDate: { type: GraphQLString },
        lastModifiedDate: { type: GraphQLString },
        slug: { type: GraphQLString },
        url: { type: GraphQLString },
        title: { type: GraphQLString },
        label: { type: GraphQLString },
        targetUrl: { type: GraphQLString },
        attributes: {
            type: ObjectType
        },
        topics: {
            type: new GraphQLList(Topic)
        },
        relatedMedia: {
            type: RelatedMedia
        },
        language: { type: GraphQLString }
    })
});

export default CoverageContainer;
