import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import TypeSchema from '../../../../../src/Schema/v1/Reference/RelatedMedia/RelatedMediaItem/Types/VideoReference';
import image from '../image.fixture.json';
import video from '../video.fixture.json';

const logStub = stub();

const loadStub = stub();

loadStub.withArgs('video_test')
    .returns(video[0]);

loadStub.withArgs('image_test')
    .returns(image[0]);

const qopts = {
    db: {
        resolveRef(parent) {
            return loadStub(parent.referenceId);
        },
        resolveHandle(parent) {
            return loadStub(parent.resource.handle.referenceId);
        }
    },
    log: logStub
};

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));


describe('RelatedMediaItem VideoReference Schema Tests', function() {
    describe('Basic schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                videoReference: {
                    type: TypeSchema,
                    resolve() {
                        return {
                            id: 'video_test',
                            referenceId: 'video_test',
                            type: 'reference',
                            referenceType: 'video',
                            extensionAttributes: {
                                branding: 'test branding'
                            },
                            resource: {
                                handleType: 'reference',
                                handle: {
                                    type: 'image',
                                    referenceId: 'image_test'
                                }
                            }
                        };
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });

        it('should return the video as a hydrated reference', async function() {
            const query = `
                query VideoReference {
                    videoReference {
                        referenceId
                        type
                        referenceType
                        reference {
                            id
                            type
                        }
                        extensionAttributes {
                            branding
                        }
                        resource {
                            ...on Image {
                                id
                                type
                            }
                        }
                    }
                }
            `;

            const expected = {
                data: {
                    videoReference: {
                        referenceId: 'video_test',
                        type: 'reference',
                        referenceType: 'video',
                        reference: {
                            id: 'video_test',
                            type: 'video'
                        },
                        extensionAttributes: {
                            branding: 'test branding'
                        },
                        resource: {
                            id: 'image_test',
                            type: 'image'
                        }
                    }
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });

    });

});
