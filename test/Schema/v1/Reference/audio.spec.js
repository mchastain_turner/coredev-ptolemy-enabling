import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './audio.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Audio';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub
    }
};

describe('Didgeridoo Audio Schema Tests', function() {
    describe('Basic Audio schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                audio: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an audio', async function() {
            const query = `
                query Audio {
                    audio {
                        id
                        type
                    }
                }
            `;

            const expected = {
                audio: {
                    id: 'audio_test',
                    type: 'audio'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an audio', async function() {
            const query = `
                query Audio {
                    audio {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        headline
                        description {
                            ...ParagraphFormatFragment
                        }
                        attributes
                        transcript {
                            ...ParagraphFormatFragment
                        }
                        fileLocation
                        section
                        branding
                        show
                        podcastFeed
                        audioProfiles
                        subsite
                        audioLength
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideoCollection
                            hasInteractive
                            media {
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    sendToApps
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    type
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                }
                                ...on ImageValue {
                                    id
                                    imageId
                                    type
                                    referenceUrl
                                    referenceUri
                                    location
                                    slug
                                    dam_id
                                    photographer
                                    caption
                                    cuts
                                }
                                ...on ArticleReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                            }
                            autoZone {
                                auto
                            }
                        }
                        language
                    }
                }
                fragment ParagraphFragment on Paragraph {
                    id
                    plaintext
                    richtext
                    elements {
                        ...on Handle {
                            type
                            attributes
                            target {
                                type
                                subtype
                                referenceUrl
                                referenceUri
                            }
                        }
                        ...on Embed {
                            type
                            attributes
                        }
                    }
                }
                fragment ParagraphFormatFragment on Paragraph {
                    ...ParagraphFragment
                    format
                }
            `;
            const expected = {
                data: {
                    audio: fixture[0]
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });


    });

});
