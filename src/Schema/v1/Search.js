import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList
} from 'graphql';

import Reference from './Reference';

const Search = new GraphQLObjectType({
    name: 'Search',
    fields: () => ({
        status: { type: GraphQLInt },
        copyright: { type: GraphQLString },
        results: { type: GraphQLInt },
        links: {
            type: new GraphQLObjectType({
                name: 'SearchLinks',
                fields: () => ({
                    nextUri: { type: GraphQLString },
                    prevUri: { type: GraphQLString },
                    selfUri: { type: GraphQLString }
                })
            })
        },
        start: { type: GraphQLInt },
        docs: { type: new GraphQLList(Reference) }
    })
});

export default Search;
