/* eslint no-shadow:0 */
import chai from 'chai';
import {stub, match} from 'sinon';
import http from 'http';
import {createReadStream} from 'fs';

import HttpPlugin from '../../src/Plugins/HttpPlugin';

const should = chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-properties'));

const statsdClient = {
    timing: stub(),
    increment: stub(),
    histogram: stub()
};

describe('HttpPlugin', function() {
    describe('register', function() {
        it('should have the name attribute', function() {
            HttpPlugin.register.should.be.a('function')
                .and.have.property('attributes')
                .and.have.property('name', 'HttpPlugin');
        });

        it('should call server.ext onRequest when registering', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub()
            };

            HttpPlugin.register(serverStub, {}, function() {
                try {
                    serverStub.ext.should.have.been.calledOnce
                        .and.calledWith('onRequest', match.func);
                    done();
                } catch(err) {
                    done(err);
                }
            });
        });
    });

    describe('openConnection', function() {
        it('should attach a connection to req.db', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub()
            };

            HttpPlugin.register(serverStub, {}, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    reply.should.not.have.been.called;
                    reply.continue.should.have.been.calledOnce;

                    req.db.should.be.an('object');

                    req.db.should.have.property('getSearch')
                        .and.be.a('function');

                    req.db.should.have.property('getSection')
                        .and.be.a('function');

                    req.db.should.have.property('getDocs')
                        .and.be.a('function');

                    done();
                } catch(err) {
                    done(err);
                }
            });
        });

        it('should call the reply function with an error if there is an exeption', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub()
            };

            HttpPlugin.register(serverStub, {}, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = function() {
                        throw Error('test exception');
                    };

                    openConnection(req, reply);

                    reply.should.have.been.calledOnce;
                    reply.getCall(0).args[0].should.be.an.instanceOf(Error)
                        .and.have.property('message', 'test exception');

                    done();
                } catch(err) {
                    done(err);
                }
            });
        });
    });

    describe('getSection', function() {
        it('should return a promise that calls http.get', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub(),
                plugins: {
                    statsdClient
                }
            };

            const reqStub = {
                on: stub()
            };

            const responseObj = createReadStream('./test/Plugins/sample.json');
            responseObj.statusCode = 200;
            responseObj.headers = {};

            const getStub = stub(http, 'get')
                .callsArgWith(1, responseObj)
                .returns(reqStub);

            HttpPlugin.register(serverStub, {
                hostname: 'foo',
                port: 1234,
                basePath: '/bar'
            }, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    req.db.getSection.should.be.a('function');
                    req.db.getSection('homepage')
                    .then(
                        function(result) {
                            try {
                                result.should.be.an('object')
                                    .and.have.property('foo', 'bar');

                                getStub.should.have.been.calledOnce;
                                getStub.getCall(0).args[0].should.be.an('object')
                                    .and.have.properties({
                                        hostname: 'foo',
                                        port: 1234,
                                        path: '/bar/composites/sections/homepage'
                                    });
                                http.get.restore();
                                done();
                            } catch(err) {
                                done(err);
                            }
                        },
                        function(err) {
                            try {
                                (err === null).should.be.true;
                                done();
                            } catch(err) {
                                done(err);
                            }
                        }
                    )
                    .catch(function(err) {
                        done(err);
                    });
                } catch(err) {
                    done(err);
                }
            });
        });

        it('should return a promise that calls http.get and fails the promise properly', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub(),
                plugins: {
                    statsdClient
                }
            };

            const reqStub = {
                on: stub()
            };

            const responseObj = createReadStream('./test/Plugins/badsample.json');
            responseObj.statusCode = 200;
            responseObj.headers = {};

            const getStub = stub(http, 'get')
                .callsArgWith(1, responseObj)
                .returns(reqStub);

            HttpPlugin.register(serverStub, {
                hostname: 'foo',
                port: 1234,
                basePath: '/bar'
            }, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    req.db.getSection.should.be.a('function');
                    req.db.getSection('homepage')
                    .then(
                        function(result) {
                            try {
                                should.not.exist(result);
                                done();
                            } catch(err) {
                                done(err);
                            }
                        },
                        function(err) {
                            try {
                                getStub.should.be.calledOnce;
                                err.should.exist
                                    .and.is.an.instanceOf(Error);
                                http.get.restore();
                                done();
                            } catch(err) {
                                done(err);
                            }
                        }
                    )
                    .catch(function(err) {
                        done(err);
                    });
                } catch(err) {
                    done(err);
                }
            });
        });
    });

    describe('getDocs', function() {
        it('should return a promise that calls http.get', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub(),
                plugins: {
                    statsdClient
                }
            };

            const reqStub = {
                on: stub()
            };

            const responseObj = createReadStream('./test/Plugins/singledoc.json');
            responseObj.statusCode = 200;
            responseObj.headers = {};

            const getStub = stub(http, 'get')
                .callsArgWith(1, responseObj)
                .returns(reqStub);

            HttpPlugin.register(serverStub, {
                hostname: 'foo',
                port: 1234,
                basePath: '/bar'
            }, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    req.db.getDocs.should.be.a('function');
                    req.db.getDocs('article_test')
                    .then(
                        function(result) {
                            try {
                                result.should.be.an('array')
                                    .and.have.length(1);

                                result[0].should.have.property('foo', 'bar');

                                getStub.should.have.been.calledOnce;
                                getStub.getCall(0).args[0].should.be.an('object')
                                    .and.have.properties({
                                        hostname: 'foo',
                                        port: 1234,
                                        path: '/bar/docs/article_test'
                                    });
                                http.get.restore();
                                done();
                            } catch(err) {
                                done(err);
                            }
                        },
                        function(err) {
                            try {
                                should.not.exist(err);
                                done();
                            } catch(err) {
                                done(err);
                            }
                        }
                    )
                    .catch(function(err) {
                        done(err);
                    });
                } catch(err) {
                    done(err);
                }

            });
        });

        it('should return a promise that calls http.get and fails the promise properly', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub(),
                plugins: {
                    statsdClient
                }
            };

            const reqStub = {
                on: stub()
            };

            const responseObj = createReadStream('./test/Plugins/badsample.json');
            responseObj.statusCode = 200;
            responseObj.headers = {};

            const getStub = stub(http, 'get')
                .callsArgWith(1, responseObj)
                .returns(reqStub);

            HttpPlugin.register(serverStub, {
                hostname: 'foo',
                port: 1234,
                basePath: '/bar'
            }, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    req.db.getDocs.should.be.a('function');
                    req.db.getDocs('article_test')
                    .then(
                        function(result) {
                            try {
                                should.not.exist(result);
                                done();
                            } catch(err) {
                                done(err);
                            }
                        },
                        function(err) {
                            try {
                                getStub.should.be.calledOnce;
                                err.should.exist
                                    .and.is.an.instanceOf(Error);
                                http.get.restore();
                                done();
                            } catch(err) {
                                done(err);
                            }
                        }
                    )
                    .catch(function(err) {
                        done(err);
                    });
                } catch(err) {
                    done(err);
                }
            });
        });
    });

    describe('getSearch', function() {
        it('should return a promise that calls http.get', function(done) {
            const serverStub = {
                ext: stub(),
                log: stub(),
                plugins: {
                    statsdClient
                }
            };

            const reqStub = {
                on: stub()
            };

            const responseObj = createReadStream('./test/Plugins/singledoc.json');
            responseObj.statusCode = 200;
            responseObj.headers = {};

            const getStub = stub(http, 'get')
                .callsArgWith(1, responseObj)
                .returns(reqStub);

            HttpPlugin.register(serverStub, {
                hostname: 'foo',
                port: 1234,
                basePath: '/bar'
            }, function() {
                try {
                    const openConnection = serverStub.ext.getCall(0).args[1];
                    const req = {};

                    const reply = stub();
                    reply.continue = stub();

                    openConnection(req, reply);

                    req.db.getSearch.should.be.a('function');
                    req.db.getSearch('id:article_test')
                    .then(
                        function(result) {
                            try {
                                result.should.be.an('array')
                                    .and.have.length(1);

                                result[0].should.have.property('foo', 'bar');

                                getStub.should.have.been.calledOnce;
                                getStub.getCall(0).args[0].should.be.an('object')
                                    .and.have.properties({
                                        hostname: 'foo',
                                        port: 1234,
                                        path: '/bar/search/id:article_test'
                                    });
                                http.get.restore();
                                done();
                            } catch(err) {
                                done(err);
                            }
                        },
                        function(err) {
                            try {
                                should.not.exist(err);
                                done();
                            } catch(err) {
                                done(err);
                            }
                        }
                    )
                    .catch(function(err) {
                        done(err);
                    });
                } catch(err) {
                    done(err);
                }

            });
        });

    });

});
