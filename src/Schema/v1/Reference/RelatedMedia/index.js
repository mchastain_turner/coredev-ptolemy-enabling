import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLBoolean
} from 'graphql';

import AutoZone from '../Subs/AutoZone';
import RelatedMediaItem from './RelatedMediaItem';
import RelatedMediaZone from './RelatedMediaZone';

function addCardsToParent(relatedMedia) {
    if ( relatedMedia && Array.isArray(relatedMedia.media)) {
        const zoneTypeMap = {
            card: 'zone',
            container: 'container'
        };
        relatedMedia = relatedMedia.media.reduce((acc, v) => {
            // The bucket will be relateMedia.zone or .container
            const bucket = acc[zoneTypeMap[v.parentType]];
                if( bucket && bucket[v.parent] ) {
                    const parent = bucket[v.parent];
                    if(!parent.children) {
                        parent.children = [];
                    }
                    parent.children.push(v);
                }
            return acc;
            }, relatedMedia);
    }
    return relatedMedia;
}

function addContainersToZone(relatedMedia) {

    if( relatedMedia.container && relatedMedia.zone ) {

        for( const c in relatedMedia.container) {
            const container = relatedMedia.container[c];
            const bucket = relatedMedia.zone[container.parent];
            if( bucket ) {
                if(!bucket.children) {
                    bucket.children = [];
                }
                bucket.children.splice(container.weight, 0, container);
            }
        }
    }

    return relatedMedia;
}

function addChildrenToZone(relatedMedia) {
    relatedMedia = addCardsToParent(relatedMedia);
    relatedMedia = addContainersToZone(relatedMedia);

    return relatedMedia;
}

function resolveZones(parent, {ids}) {
    parent = addChildrenToZone(parent);

    // map the zones object into an array and filter it
    return Object.keys(parent.zone)
        .map(key => parent.zone[key])
        .filter((zone) => {
            if (!ids || ( ids.includes(zone.id)) ) {
                return true;
            }
            return false;
        });
}

const RelatedMedia = new GraphQLObjectType({
    name: 'RelatedMedia',
    fields: () => ({
        hasImage: { type: GraphQLBoolean },
        hasGallery: { type: GraphQLBoolean },
        hasVideo: { type: GraphQLBoolean },
        hasVideo360: { type: GraphQLBoolean },
        hasVideoCollection: { type: GraphQLBoolean },
        hasInteractive: { type: GraphQLBoolean },
        media: {
            type: new GraphQLList(RelatedMediaItem)
        },
        zone: {
            args: {
                ids: {type: new GraphQLList(GraphQLString) }
            },
            type: new GraphQLList(RelatedMediaZone),
            description: 'Returns an array of zones which' +
                ' can be queried with an "ids" agrument',
            resolve: resolveZones
        },
        autoZone: {
            type: AutoZone
        }
    })
});

export default RelatedMedia;
