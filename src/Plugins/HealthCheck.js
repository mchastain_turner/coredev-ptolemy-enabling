
/**
 * Return Healthcheck response
 * @param req HAPI request
 * @param res HAPI response
 */
function getRouteHandler(hostname, productName, version) {
    return (req, res) => {
        return res({
            status: 'UP',
            hostname,
            productName,
            version
        });
    };
}

/**
 * Register this plugin with the caller
 * @param  {Object}   server  Server object to register with
 * @param  {Object}   options Options to use to init this plugin
 * @param  {Function} next    Callback
 */
function register(server, options, next) {
    server.route({
        method: 'GET',
        path: '/_healthcheck',
        handler: getRouteHandler(
            options.hostname,
            options.productName,
            options.version
        )
    });
    next();
}

register.attributes = {
    name: 'HealthCheck'
};

const HealthCheck = {register};

export default HealthCheck;
