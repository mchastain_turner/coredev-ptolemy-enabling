
FROM node:6.2.1

WORKDIR /opt/coredev-ptolemy

ADD package.json package.json
ADD dist dist
ADD node_modules node_modules

EXPOSE 5000

CMD ["npm", "start"]
