import {
    GraphQLObjectType,
    GraphQLString} from 'graphql';

const LiveVideoValue = new GraphQLObjectType({
    name: 'LiveVideoValue',
    fields: () => ({
        type: { type: GraphQLString },
        liveStream: { type: GraphQLString },
        playVidLiveStream: { type: GraphQLString }
    })

});

export default LiveVideoValue;
