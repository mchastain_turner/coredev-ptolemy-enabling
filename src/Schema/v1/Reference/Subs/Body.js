import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import Paragraph from '../../Paragraph';

const Body = new GraphQLObjectType({
    name: 'Body',
    fields: () => ({
        footer: {
            type: new GraphQLObjectType({
                name: 'BodyFooter',
                fields: () => ({
                    type: { type: GraphQLString },
                    text: { type: GraphQLString },
                    paragraphs: {
                        type: new GraphQLList(Paragraph)
                    }
                })
            })
        },
        notes: {
            type: new GraphQLObjectType({
                name: 'BodyNotes',
                fields: () => ({
                    www: { type: GraphQLString },
                    intl: { type: GraphQLString },
                    wwwText: {
                        type: new GraphQLList(Paragraph)
                    },
                    intlText: {
                        type: new GraphQLList(Paragraph)
                    },
                    type: { type: GraphQLString }
                })
            })
        },
        paragraphs: {
            type: new GraphQLList(Paragraph)
        },
        wordCount: { type: GraphQLInt }
    })
});

export default Body;
