# ---------------------------------------------------------
# THIS IS ONLY INTENDED TO BE USED FOR LOCAL DEVELOPMENT!!!
# ---------------------------------------------------------

PORT=5000
DOCKER_IP=`docker-machine env default --no-proxy | sed -n 's/export NO_PROXY="\([.0-9]*\)"/\1/p'`
NPM_CMD=start
RUN_SCRIPT=

ifdef inspect
NPM_CMD="inspect"
RUN_SCRIPT="run-script"
endif

dockerbuild:
	@npm install && docker build -t vgtf/coredev-ptolemy .

dockerrun:
	@docker run -ti --rm \
	-p ${PORT}:${PORT} \
	--env-file dev.env \
	-e "PORT=${PORT}" \
	vgtf/coredev-ptolemy

start:
	@grunt build
	@DATA_SERVICE_HOSTNAME=compositor.api.cnn.com \
	DATA_SERVICE_PORT=80 \
	DATA_SERVICE_PATH=/svc/mcs/v3 \
	DATA_SERVICE_TIMEOUT=2000 \
	BEARER_TOKEN=eyJhbGciOiJIUzI1NiJ9.Y29yZWRldi1wdG9sZW15L2xvY2Fs.ABdKjddj0G6gc6ruFtLv5Thnrys9IhSLOJyW6cadsnM \
	MAX_BATCH_SIZE=10 \
	LOADER_CACHE_ENABLED=1 \
	LOADER_CACHE_TTL=500 \
	STATSD_HOST=udp://${DOCKER_IP}:8125 \
	ENVIRONMENT=localhost \
	HOST=localhost \
	PORT=${PORT} \
	npm ${RUN_SCRIPT} ${NPM_CMD}

startdev:
	@grunt build
	@DATA_SERVICE_HOSTNAME=10.60.184.26 \
	LOG_DEBUG=1 \
	LOG_OPS=1 \
	DATA_SERVICE_PORT=8080 \
	DATA_SERVICE_PATH=/svc/mcs/v3 \
	DATA_SERVICE_TIMEOUT=2000 \
	BEARER_TOKEN=eyJhbGciOiJIUzI1NiJ9.Y29yZWRldi1wdG9sZW15L2xvY2Fs.ABdKjddj0G6gc6ruFtLv5Thnrys9IhSLOJyW6cadsnM \
	MAX_BATCH_SIZE=50 \
	LOADER_CACHE_ENABLED=1 \
	LOADER_CACHE_TTL=500 \
	STATSD_HOST=udp://${DOCKER_IP}:8125 \
	ENVIRONMENT=localhost \
	HOST=localhost \
	PORT=${PORT} \
	npm ${RUN_SCRIPT} ${NPM_CMD}

startref:
	@grunt build
	@DATA_SERVICE_HOSTNAME=10.60.180.237 \
	LOG_DEBUG=1 \
	DATA_SERVICE_PORT=8080 \
	DATA_SERVICE_PATH=/svc/mcs/v3 \
	DATA_SERVICE_TIMEOUT=2000 \
	BEARER_TOKEN=eyJhbGciOiJIUzI1NiJ9.Y29yZWRldi1wdG9sZW15L2xvY2Fs.ABdKjddj0G6gc6ruFtLv5Thnrys9IhSLOJyW6cadsnM \
	MAX_BATCH_SIZE=50 \
	LOADER_CACHE_ENABLED=1 \
	LOADER_CACHE_TTL=500 \
	STATSD_HOST=udp://${DOCKER_IP}:8125 \
	ENVIRONMENT=localhost \
	HOST=localhost \
	PORT=${PORT} \
	npm ${RUN_SCRIPT} ${NPM_CMD}

startintegration:
	@grunt build
	@DATA_SERVICE_HOSTNAME=compositor-int.api.cnn.com \
	LOG_DEBUG=1 \
	DATA_SERVICE_PORT=80 \
	DATA_SERVICE_PATH=/svc/mcs/v3 \
	DATA_SERVICE_TIMEOUT=2000 \
	BEARER_TOKEN=eyJhbGciOiJIUzI1NiJ9.Y29yZWRldi1wdG9sZW15L2xvY2Fs.ABdKjddj0G6gc6ruFtLv5Thnrys9IhSLOJyW6cadsnM \
	MAX_BATCH_SIZE=50 \
	LOADER_CACHE_ENABLED=1 \
	LOADER_CACHE_TTL=10000 \
	STATSD_HOST=udp://${DOCKER_IP}:8125 \
	ENVIRONMENT=localhost \
	HOST=localhost \
	PORT=${PORT} \
	npm ${RUN_SCRIPT} ${NPM_CMD}

install:
	npm install
build:
	npm build
test:
	npm test

.PHONY: test
