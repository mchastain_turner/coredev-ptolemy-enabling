import chai from 'chai';
import {stub} from 'sinon';
import {
    graphql,
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import fixture from './special.fixture.json';
import TypeSchema from '../../../../src/Schema/v1/Reference/Special';

chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-properties'));

const getStub = stub();

getStub.returns(
    Promise.resolve([])
);

const qopts = {
    db: {
        getDoc: getStub,
        getDocs: getStub
    }
};

describe('Didgeridoo Special Schema Tests', function() {
    describe('Basic Special schema test', function() {
        const Query = new GraphQLObjectType({
            name: 'Query',
            fields: () => ({
                special: {
                    type: TypeSchema,
                    resolve() {
                        return fixture[0];
                    }
                }
            })
        });

        const Schema = new GraphQLSchema({
            query: Query
        });
        it('should return a field on an special', async function() {
            const query = `
                query Special {
                    special {
                        id
                        type
                    }
                }
            `;

            const expected = {
                special: {
                    id: 'special_test',
                    type: 'special'
                }
            };

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal( { data: expected });
        });

        it('should return all fields on an special', async function() {
            const query = `
                query Special {
                    special {
                        id
                        sourceId
                        type
                        dataSource
                        schema
                        schemaVersion
                        status {state}
                        firstPublishDate
                        lastPublishDate
                        lastModifiedDate
                        slug
                        url
                        attributes
                        branding
                        section
                        topicHead
                        topicIntro
                        title
                        topics {
                            label
                            class
                            id
                            topicID
                            confidenceScore
                            leafNode
                            parentPathIds
                        }
                        relatedMedia {
                            hasImage
                            hasGallery
                            hasVideo
                            hasVideoCollection
                            hasInteractive
                            zone {
                                limit
                                zoneType
                                displayLabel
                                label
                                showAds
                                id
                                priority
                                branding
                                options
                            }
                            media {
                                ...on VideoReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    cvpXmlUrl
                                    autoplay
                                    duration
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    sendToApps
                                    auxiliaryText
                                }
                                ...on VideoCollectionReference {
                                    id
                                    referenceId
                                    type
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    location
                                    url
                                    cuts
                                }
                                ...on ImageValue {
                                    id
                                    imageId
                                    type
                                    referenceUrl
                                    referenceUri
                                    location
                                    slug
                                    dam_id
                                    photographer
                                    caption
                                    cuts
                                }
                                ...on ArticleReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                                ...on CoverageContainerReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on HyperlinkReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on InteractiveReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    sendToApps
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    cuts
                                    auxiliaryText
                                }
                                ...on GalleryReference {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                }
                                ...on ValueCard {
                                    id
                                    referenceId
                                    type
                                    location
                                    headline
                                    description {
                                        ...ParagraphFormatFragment
                                    }
                                    banner {
                                        ...ParagraphFormatFragment
                                    }
                                    url
                                    referenceType
                                    referenceUrl
                                    referenceUri
                                    sendToApps
                                    cuts
                                    auxiliaryText
                                    target{
                                        ... on LiveVideoValue{
                                            type
                                            liveStream
                                            playVidLiveStream
                                        }
                                    }
                                   valueType
                               }
                            }
                            autoZone {
                                auto
                            }
                        }
                        language
                    }
                }
                fragment ParagraphFragment on Paragraph {
                    id
                    plaintext
                    richtext
                    elements {
                        ...on Handle {
                            type
                            attributes
                            target {
                                type
                                subtype
                                referenceUrl
                                referenceUri
                            }
                        }
                        ...on Embed {
                            type
                            attributes
                        }
                    }
                }
                fragment ParagraphFormatFragment on Paragraph {
                    ...ParagraphFragment
                    format
                }
            `;

            const expectedFixtureCopy = JSON.parse(JSON.stringify(fixture[0]));
            expectedFixtureCopy.relatedMedia.zone = Object.keys(expectedFixtureCopy.relatedMedia.zone)
                .map((key) => expectedFixtureCopy.relatedMedia.zone[key]);
            const expected = {
                data: {
                    special: expectedFixtureCopy
                }
            };

            /**
             * For now we are deleting these fields out of expected, since they
             * are not currently supported in the schema and are likely
             * to change anyways.
             */
            function deleteUnsupported(item) {
                delete item.branding;
                delete item.brandingOn;
                delete item.captions;
                delete item.contributorProfiles;
                delete item.dateCreated;
                delete item.flag;
                delete item.flagColor;
                delete item.kicker;
                delete item.sponsorship;
                delete item.extensionAttributes;
                delete item.resource;
            }

            expected.data.special.relatedMedia.media.forEach(deleteUnsupported);

            // also need to hoist the data from the handle in the live video value card.
            expected.data.special.relatedMedia.media[47].target = expected.data.special.relatedMedia.media[47].target.handle;

            const result = await graphql(Schema, query, qopts);
            result.should.to.deep.equal(expected);
        });


    });

});
